
<script>
var message = ""
	<s:if test="hasActionMessages()">
		<s:iterator value="actionMessages">
			+"<s:property escape="false"/>"+"\n"
		</s:iterator>
	</s:if>
	<s:if test="hasActionErrors()">
		<s:iterator value="actionErrors">
			+"*<s:property escape="false"/>"+"*\n"
		</s:iterator>
	</s:if>
	;
	
	if(message!=""){
		alert(message);
	}
</script>
