<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.atos.user.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Form Centre</title>

<script type="text/javascript">
	//for jqGrid
	$(window).resize(function(){
		$("#grid").jqGrid('setGridWidth',$(window).width()-230);
	});
	
	var selectedId = "";
	var dataList = '';
	$(document).ready(function() {
		staff();
		
		$("select#selectChoice").change(function(){ 
			getSelection();
		});
		
	});
	
	function staff() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/user/ListUserConditionAction.do?userId=${userId}&deleteFlag=Y'),
			datatype: "JSON",
			height: "280", 
			width:$(window).width()-100,
			//idPrefix: "staff_",
			colNames:['使用者帳號', '密碼', '姓名', '角色','手機', '電郵', '新增日', 'D', '通知群組', '備註', '刪除'],
	        colModel:[
						{name:'userId', index:'userId', width:30, editable: true, align:'center',
							  editrules:{required:true}, editoptions:{maxlength:10}, 
							  search:true, searchoptions:{sopt:['eq', 'cn']}
						}, 
						{name:'password', index:'password', hidden: true},
						{name:'name', index:'name', hidden: true},
						{name:'role', index:'role', width:25, editable: true, align:'center', 
							  editoptions:{size:32, maxlength: 30}, 
							  search:false, searchoptions:{sopt:['eq', 'cn']},
							  editrules:{required: true},
							  edittype:'select', editoptions:{value:{21:'ADMIN',20:'USER'}} 
						},
						{name:'mobile', index:'name', width:25, align:'center', editable: true,
							  editoptions:{size:12, maxlength: 10}, formatter:formatPhoneNumber,
							  search:false, searchoptions:{sopt:['eq', 'cn']},
							  editrules:{required: true}
						},
						{name:'email', index:'name', width:40, align:'right', editable: true,
							  editoptions:{size:32, maxlength: 50}, formatter:'email',
							  search:false, searchoptions:{sopt:['eq', 'cn']},
							  editrules:{required: true}
						},
						{name:'createDate', index:'createDate', hidden: true},
						{name:'deleteFlag', index:'deleteFlag', hidden: true},
						{name:'notifyGroup', index:'notifyGroup', width:20, editable: true, align:'right', 
							  editoptions:{size:32, maxlength: 30}, 
							  search:false, searchoptions:{sopt:['eq', 'cn']},
							  editrules:{required: true},
							  edittype:'select', 
							  editoptions:{value:{1:'OB', 2:'LB', 3:'SB', 4:'OB,LB', 5:'OB,SB', 6:'SB,LB', 7:'OB,LB,SB'}}
						},
						{name:'note', index:'note', width:50, editable: true, align:'center', 
							  editoptions:{size:32, maxlength: 30}, 
							  search:false, searchoptions:{sopt:['eq', 'cn']},
							  editrules:{required: true}
						},
           				{name:'deleteFlag', index:'deleteFlag', editable: true, align:'center',
         	  				  editrules:{required: true}, width:10, 
         	                  edittype:'select', editoptions:{value:{'Y':'Y','N':'N'}} 
                        },
	                  
	        ],
	        sortorder: 'asc',  
	        sortname:'userId',
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 10, 
	        rowList: [10,20], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "已刪除員工管理"
	    });
		
		jQuery("#grid").jqGrid('navGrid','#gridPager', 
			{edit:true, add:false, del:false, search:false, refresh:false, 
			     edittext: "更新資料", edittitle: "更新資料", 
			     addtext: "新增資料", addtitle: "新增資料", 
			     deltext: "刪除資料", deltitle: "刪除資料",
			     searchtext: "搜尋", searchtitle: "搜尋", 
			     refreshtext: "重新整理", refreshtitle: "重新整理"
			},
			
			// edit options
			{ url: '${pageContext.request.contextPath}/user/ModifyUserAction.do?modifyUserAct=recovery',
			  height:320,
			  width:500,
			  //viewPagerButtons: false, //remove previous/next arrow
              reloadAfterSubmit: true,
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','userId',{editoptions:{readonly:'readonly'}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/user/ModifyUserAction.do?modifyUserAct=add',
			  height:320,  
			  width:500,	
		      reloadAfterSubmit:true,
		      closeAfterAdd: true,
		      recreateForm: true,
              beforeInitData: function () {
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/user/ModifyUserAction.do?modifyUserAct=delete',
			  
			  delData: {
				  userId: function() {
				      var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
                      var value = $("#grid").jqGrid('getCell', selectedId, 'userId');
                      alert(value);
                      return value;
			      }
			  },
			  reloadAfterSubmit: false
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/user/JqGridFindAllStaff.do?UserType=STAFF'
			} 
		)
		/* .navButtonAdd('#gridPager', { 
			 caption: "測試按鈕", 
			 buttonicon: "ui-icon-person", 
			 onClickButton: function() { 
				   setModerator();
				 }, 
			 position: "last",
			 reloadAfterSubmit:false
			 });
		$('#testTable').hide(); */
	}
	

    function setModerator(){
		
		var id = $('#grid').jqGrid('getGridParam','selrow');
		
		if(id == null){
			alert('請選擇user!');
			return;			
		}
		
		var data = jQuery("#grid").jqGrid('getRowData', id);
		
		$('#showUserId').html("<font color='red'>" + data.userId + "</font>");
		$('#testTable').show();
		
		/* 
		$.ajax({
			type : "POST",
			url : "${pageContext.request.contextPath}/discuss/getBoardModerator.do",
			dataType : "json",
			data : {boardId : data.boardId},
			cache : false,
			success : function(dataList) {
				if (dataList != null) {
					var dataArray = new Array();
					$.each(dataList,function(i,item){
						
						//var dataValue = dataList[i].id.account;
						//dataArray[i] = dataValue ;
						dataArray[i] = dataList[i].id.account;
					});
					$("input:checkbox").val(dataArray);
				}	
			}
		});
		 */
	}
	
	function getSelection() {
		var choice = $('#selectChoice option:selected').val();
		if (choice == 1) {
			$('#staff').css("display","");
			$('#bank').css("display","none");
		} else if (choice == 2) {
			$('#staff').css("display","none");
			$('#bank').css("display","");
		}
	} 
	
	// Kent 2016/03/01
	// converts mobile phone number to (xxx)xxx-xxxx or xxx-xxx-xxxx...
	function formatPhoneNumber(cellvalue, options, rowObject) {
	    var re = /\D/;
	    // test for this format: (xxx)xxx-xxxx
	    var re2 = /^\({1}\d{3}\)\d{3}-\d{4}/;
	    // test for this format: xxx-xxx-xxxx
	    //var re2 = /^\d{3}-\d{3}-\d{4}/;
	    var num = cellvalue;
	    if (num === null){
	        num = "";
	    }
	    var newNum = num;
	    if (num != "" && re2.test(num) != true) {
	        if (num != "") {
	            while (re.test(num)) {
	                num = num.replace(re, "");
	            }
	        }
	        if (num.length == 10) {
	            // for format (xxx)xxx-xxxx
	            //newNum = '(' + num.substring(0, 3) + ')' + num.substring(3, 6) + '-' + num.substring(6, 10);
	            // for format xxx-xxx-xxxx
	            // newNum = num.substring(0,3) + '-' + num.substring(3,6) + '-' + num.substring(6,10);
	            // for format xxxx-xxxxxx
	            //newNum = num.substring(0,4) + '-' + num.substring(4,10);
	            // for format xxxxxxxxxx
	            newNum = num.substring(0,4)  + num.substring(4,10);
	        }
	    }
	    return newNum;
	}
</script>

</head>
<body>
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/tpe_logo.png" alt="" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br><br><br>
<div class="path">
PATH： 使用者管理 :
使用者列表/查詢/修改</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>
<form action="" method="post" name="form1" id="form1">

<table width="400">
  <tr>
    <th>使用者帳號(User ID)：</th>
    <td width="50">
      <input name="userId" id="userId" type="text" size="20" maxlength="20" value=""  lang="en" />
    </td>
    <td>
    <s:submit onclick="searchUser()" cssClass="comfirm" value="查詢" theme="simple"/>
    </td>
  </tr>
</table>
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
<!--   <table id="testTable">
	 <td>
	   選擇使用者帳號(User ID) : <span id="showUserId"></span>
	 </td>
  </table> -->
</form>
</div>
</div>
</div>
</body>
</html>
 