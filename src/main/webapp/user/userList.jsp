<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.atos.user.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Form Management- Manage Users </title>

<script type="text/javascript">
    
	//for jqGrid
	$(window).resize(function(){
		$("#grid").jqGrid('setGridWidth',$(window).width()-320);
	});
	
	var selectedId = "";
	var dataList = '';
	$(document).ready(function() {
		staff();
		
		$("select#selectChoice").change(function(){ 
			getSelection();
		});
		
	});
	
	function staff() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/user/ListUserConditionAction.do?userId=${userId}&country=${country}&countryAbbr=${countryAbbr}&deleteFlag=N'),
			
			datatype: "JSON",
			height: "280", 
			width:$(window).width()-180,
			colNames:['Account', 'Contact Name', 'Country [CountryAbbr.]','Role','Tel', 'Email', '新增日', 'D', 'comment', 'Password'],
	        colModel:[
	                  {name:'userId', index:'userId', width:40, align:'center',
	                	  editable:true , editoptions:{size:40, maxlength: 40},
	                	  search:false, editrules:{required: true}   
	                  }, 
	                  {name:'name', index:'name',width:45, editable: true,
	                	  editoptions:{size:40, maxlength: 40},
	                	  search:false, editrules:{required: true}  
	                  },
	                  {name:'country', index:'country',width:100,
	                	  editable:true , editoptions:{size:40, maxlength: 40},
	                	  search:false, editrules:{required: true}  
	                  },
	                  {name:'role', index:'role', width:10, editable: true, align:'center', hidden: true},
	                  {name:'mobile', index:'mobile', width:60, align:'center', editable: true,
	                	  editoptions:{size:20, maxlength: 20}
	                  },
	                  {name:'email', index:'email', width:60, align:'right', editable: true,
	                	  editoptions:{size:50, maxlength: 50}, formatter:'email',
	                	  search:false, editrules:{required: true}
	                  },
	                  {name:'createDate', index:'createDate', hidden: true},
	                  {name:'deleteFlag', index:'deleteFlag', hidden: true},
	                  {name:'note', index:'note', width:30, editable: true, align:'center', 
	                	  editoptions:{size:50, maxlength: 50},
	                  },
	                  {name:'password', index:'password', width:1, 
	                	  editable: true, editoptions:{size:20, maxlength: 20}, formatter:"password",
	                	  edittype:"password", editrules:{required:true}  
	                  },
	        ],
	        //sortorder: 'asc',
	        //sortname:'u.userId',
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 10, 
	        rowList: [10,20], 
	        pgbuttons: true,
	        pginput: true,
	        toppager: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "Accout Management"
	    });
		
		jQuery("#grid").jqGrid('navGrid','#gridPager', 
			{edit:true, add:false, del:true, search:false, refresh:false, cloneToTop:true,
			     edittext: "Modify", edittitle: "Modify", 
			     addtext: "Add", addtitle: "Add", 
			     deltext: "Delete", deltitle: "Delete",
			     searchtext: "搜尋", searchtitle: "搜尋", 
			     refreshtext: "重新整理", refreshtitle: "重新整理"
			},
			
			// edit options
			{ url: '${pageContext.request.contextPath}/user/ModifyUserAction.do?modifyUserAct=modify',
			  height:400,
			  width:500,
			  //viewPagerButtons: false, //remove previous/next arrow
              reloadAfterSubmit: true,
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','userId',{editoptions:{readonly:'readonly'}});
            	  $("#grid").jqGrid('setColProp','country',{editoptions:{readonly:'readonly'}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/user/ModifyUserAction.do?modifyUserAct=add',
			  height:320,  
			  width:500,	
		      reloadAfterSubmit:true,
		      closeAfterAdd: true,
		      recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','userId',{editoptions:{readonly:false}});
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/user/ModifyUserAction.do?modifyUserAct=delete',
			  
			  delData: {
				  userId: function() {
				      var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
                      var value = $("#grid").jqGrid('getCell', selectedId, 'userId');
                      alert(value);
                      return value;
			      }
			  },
			  reloadAfterSubmit: false
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/user/JqGridFindAllStaff.do?UserType=STAFF'
			} 
		)
		/* .navButtonAdd('#gridPager', { 
			 caption: "測試按鈕", 
			 buttonicon: "ui-icon-person", 
			 onClickButton: function() { 
				   setModerator();
				 }, 
			 position: "last",
			 reloadAfterSubmit:false
			 });
		$('#testTable').hide(); */
	}
	

    function setModerator(){
		
		var id = $('#grid').jqGrid('getGridParam','selrow');
		
		if(id == null){
			alert('請選擇user!');
			return;			
		}
		
		var data = jQuery("#grid").jqGrid('getRowData', id);
		
		$('#showUserId').html("<font color='red'>" + data.userId + "</font>");
		$('#testTable').show();
		
		/* 
		$.ajax({
			type : "POST",
			url : "${pageContext.request.contextPath}/discuss/getBoardModerator.do",
			dataType : "json",
			data : {boardId : data.boardId},
			cache : false,
			success : function(dataList) {
				if (dataList != null) {
					var dataArray = new Array();
					$.each(dataList,function(i,item){
						
						//var dataValue = dataList[i].id.account;
						//dataArray[i] = dataValue ;
						dataArray[i] = dataList[i].id.account;
					});
					$("input:checkbox").val(dataArray);
				}	
			}
		});
		 */
	}
	
	function getSelection() {
		var choice = $('#selectChoice option:selected').val();
		if (choice == 1) {
			$('#staff').css("display","");
			$('#bank').css("display","none");
		} else if (choice == 2) {
			$('#staff').css("display","none");
			$('#bank').css("display","");
		}
	} 
	
	// Kent 2016/03/01
	// converts mobile phone number to (xxx)xxx-xxxx or xxx-xxx-xxxx...
	function formatPhoneNumber(cellvalue, options, rowObject) {
	    var re = /\D/;
	    // test for this format: (xxx)xxx-xxxx
	    var re2 = /^\({1}\d{3}\)\d{3}-\d{4}/;
	    // test for this format: xxx-xxx-xxxx
	    //var re2 = /^\d{3}-\d{3}-\d{4}/;
	    var num = cellvalue;
	    if (num === null){
	        num = "";
	    }
	    var newNum = num;
	    if (num != "" && re2.test(num) != true) {
	        if (num != "") {
	            while (re.test(num)) {
	                num = num.replace(re, "");
	            }
	        }
	        if (num.length == 10) {
	            // for format (xxx)xxx-xxxx
	            //newNum = '(' + num.substring(0, 3) + ')' + num.substring(3, 6) + '-' + num.substring(6, 10);
	            // for format xxx-xxx-xxxx
	            // newNum = num.substring(0,3) + '-' + num.substring(3,6) + '-' + num.substring(6,10);
	            // for format xxxx-xxxxxx
	            //newNum = num.substring(0,4) + '-' + num.substring(4,10);
	            // for format xxxxxxxxxx
	            newNum = num.substring(0,4)  + num.substring(4,10);
	        }
	    }
	    return newNum;
	}
</script>

</head>
<body>
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/universiade_logo.png" alt="Universiade" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： User Managemnet :
User List</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>

<!-- Print out Search Condition on page -->
<c:if test="${(userId!='' && userId!=undefined && userId!=null)}">
		<div class="queryConditionMsg">
		   [QueryCondition(s)]<br>
		   Account：<c:out value='${userId}' default='null'/><br>
		</div>
</c:if>

<form action="" method="post" name="form1" id="form1">

<table width="400" >
  <tr>
    <th>Account：</th>
    <td width="50">
      <input name="userId" id="userId" type="text" size="20" maxlength="20" value=""  lang="en" />
    </td>
    <th>Country：</th>
    <td width="50">
      <input name="country" id="country" type="text" size="20" maxlength="20" value=""  lang="en" />
    </td>
    <th>Country Abbreviation：</th>
    <td width="50">
      <input name="countryAbbr" id="countryAbbr" type="text" size="20" maxlength="20" value=""  lang="en" />
    </td>
    <td>
    <s:submit onclick="searchUser()" cssClass="comfirm" value="Query" theme="simple"/>
    </td>
  </tr>
</table>
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
<!--   <table id="testTable">
	 <td>
	   選擇使用者帳號(User ID) : <span id="showUserId"></span>
	 </td>
  </table> -->
</form>
</div>
</div>
</div>
</body>
</html>
 