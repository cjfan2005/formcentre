<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.util.*"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/multimenu.js"></script> <!--動態選單 -->
<script language="JavaScript" src="${pageContext.request.contextPath}/js/jquery.timers-1.2.js"></script>
<script language="JavaScript" src="${pageContext.request.contextPath}/js/jquery.timers-1.2.js"></script>
<script language="JavaScript" src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>

<link href="${pageContext.request.contextPath}/css/multimenu.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
var loggedInRole = '<%= session.getAttribute("loggedInRole").toString() %>';
var loggedInUser = '<%=session.getAttribute("loggedInUser").toString()%>';

$(function(){
	
	$('div[id="idleTime"]').everyTime(1000, function(i) {
		if(i == 20*60) window.location.href = "${pageContext.request.contextPath}/logoutAction.do?isTimeOut=true";
		var min = ""+Math.floor(i/60);
		var sec = ""+i%60;
        $(this).html(((min.length == 1)?"0"+min:min)+":"+((sec.length == 1)?"0"+sec:sec));
    });
});

</script>
<!--dynamic menu Start-->
<div class="mlmenu menubg inaccesible arrow">
 
 <!-- FORM Management for Users-->
 <c:if test="${loggedInRole == 'USER' || loggedInRole == 'ROOT'}"> 
	 <ul>
	   <li>
	   	<a href="#">Form</a>
	   		<ul>
	           <li><a href="${pageContext.request.contextPath}/form/PiFormInitAction.do">PI Form</a></li>    
	       </ul>
	   </li> 
	 </ul>
 </c:if>
 
 <!-- FORM Management for ADMIN-->
 <c:if test="${loggedInRole == 'ADMIN' || loggedInRole == 'ROOT'}"> 
	  <ul>
	    <li>
	    	<a href="#">Form Management</a>
	    	<ul>
	    	    <li><a href="${pageContext.request.contextPath}/form/PiFormMagInitAction.do">PI Form</a></li>
	        </ul>
	    </li> 
	  </ul>
 </c:if>
 
 <!-- USERS Management for ADMIN-->
 <c:if test="${loggedInRole == 'ADMIN' || loggedInRole == 'ROOT'}"> 
	  <ul>
	    <li>
	    	<a href="#">User Management</a>
	    	<ul id="ulList">
	    	    <li><a href="${pageContext.request.contextPath}/user/ListUserAction.do">User List</a></li>
	    	    <!-- MASK BY KENT
	            <li><a href="${pageContext.request.contextPath}/user/ListDeletedUserAction.do">Deleted User</a></li>
	            <li><a href="${pageContext.request.contextPath}/user/GenUserAccountAction.do">Generate User Account</a></li>
	             -->
	        </ul>
	    </li> 
	  </ul>
 </c:if>

 <!-- Audit Management for Auditor-->
 <c:if test="${loggedInRole == 'AUDITOR' || loggedInRole == 'ROOT'}">
	  <ul>
	    <li>
	    	<a href="#">稽核LOG(AUDIT LOG)</a>
	    	<ul>
	            <li><a href="${pageContext.request.contextPath}/auditLog/InitAuditLogAction.do">AuditLog列表</a></li>
	        </ul>
	    </li> 
	  </ul> 
 </c:if>
</div>
<a onFocus="mlout();" href="javascript:"></a>
<noscript>TAB標籤功能，如不執行不影響您資料瀏覽</noscript>
<!--dynamic hmenu End--> 
<div class="infotb">
   <table cellspacing="5" summary="資料表格">
          <tr>
             <td class="name">Account</td>
             <td class="value">
             	<%=session.getAttribute("loggedInUser").toString()%>
             </td>
             <td class="name">Country：</td>
             <td class="value">
                <%=session.getAttribute("loggedCountry").toString()%>
                [<%=session.getAttribute("loggedCountryAbbr").toString()%>]
             </td>
          </tr>
          <tr> 
             <td class="name">Idle Time：</td>
             <td class="value"><div id="idleTime"></div></td>
             <td class="value" colspan="5"><a href="${pageContext.request.contextPath}/logoutAction.do">Sign out</a></td>
          </tr>
          
   </table>
 </div>


