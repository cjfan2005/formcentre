<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/IncludePage.jsp"%>
<title>Form Management System</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/universiade_logo.png" alt="" border="0" /></a></div>
<br />
<br />
<br />
<s:if test="hasActionMessages()">

		<div class="successMsg" align="center" style="margin:0 auto;">
		   <s:actionmessage/>
		</div>
</s:if>
<s:if test="hasActionErrors()">
		<div class="errorMsg" align="center" style="margin:0 auto;">
		   <s:actionerror/>
		</div>
</s:if>

<div class="body">

<form action="loginAuthenticaion.do" method="post" name="form1" id="form1" >
<s:hidden name="loginAttempt" value="%{'1'}" /> 
<table width="250" align="center" border="1" cellpadding="0" cellspacing="1" bordercolor="#CCCCCC">
  <tr>
    <td height="30" valign="middle" bordercolor="98B7FD" bgcolor="98B7FD" class="left-area-member-text">User authentication</td>
  </tr>
  <tr>
    <td height="44" valign="bottom" bordercolor="#FFFFFF" class="left-area-member-text">Account：
      <input name="userId" type="text" class="hintTextbox" value="enter your ID" id="userId" size="50">
    </td>
  </tr>
  <tr>
    <td height="44" valign="top" bordercolor="#FFFFFF" class="left-area-member-text">Password：
      <input name="password" type="password" class="textbox-01" id="password" size="50">
    </td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bordercolor="efefef" bgcolor="#EFEFE0" class="left-area-member-text">
      <input name="login" type="submit" class="comfirm" value="Login">
      <%--  <s:submit label="Login" name="Login" /> --%>
    </td>
  </tr>
</table>
</form>
</div>

</body>
</html>