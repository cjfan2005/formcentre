/*
 Navicat Premium Data Transfer

 Source Server         : 188.166.233.106_root
 Source Server Type    : MySQL
 Source Server Version : 50627
 Source Host           : 188.166.233.106
 Source Database       : universiade

 Target Server Type    : MySQL
 Target Server Version : 50627
 File Encoding         : utf-8

 Date: 09/09/2016 09:37:55 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `Audit`
-- ----------------------------
DROP TABLE IF EXISTS `Audit`;
CREATE TABLE `Audit` (
  `Uuid` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AuditTime` datetime NOT NULL,
  `UserId` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ProgramName` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AuditType` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ClientIpAddr` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ImplFunction` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SqlLog` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '???? oldObject & newObject',
  PRIMARY KEY (`Uuid`),
  UNIQUE KEY `Uuid_UNIQUE` (`Uuid`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `CodeMap`
-- ----------------------------
DROP TABLE IF EXISTS `CodeMap`;
CREATE TABLE `CodeMap` (
  `CodeSequence` int(25) NOT NULL,
  `purpose` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note3` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note4` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note5` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note6` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note7` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note8` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note9` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note0` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`CodeSequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `CountryMap`
-- ----------------------------
DROP TABLE IF EXISTS `CountryMap`;
CREATE TABLE `CountryMap` (
  `Country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CountryAbbr` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `CountryCode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FisuMember` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `StatusMap`
-- ----------------------------
DROP TABLE IF EXISTS `StatusMap`;
CREATE TABLE `StatusMap` (
  `StatusCode` int(20) NOT NULL,
  `StatusName` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`StatusCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `USERS`
-- ----------------------------
DROP TABLE IF EXISTS `USERS`;
CREATE TABLE `USERS` (
  `userId` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(10) DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `deleteFlag` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notifyGroup` int(10) DEFAULT NULL,
  `note` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryAbbr` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`userId`),
  KEY `role` (`role`),
  KEY `role_2` (`role`),
  KEY `role_3` (`role`),
  KEY `role_4` (`role`),
  KEY `role_5` (`role`),
  KEY `role_6` (`role`),
  KEY `role_7` (`role`),
  KEY `role_8` (`role`),
  KEY `notifyGroup` (`notifyGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `piform`
-- ----------------------------
DROP TABLE IF EXISTS `piform`;
CREATE TABLE `piform` (
  `Country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CountryAbbr` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `CountryCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `NameoftheNUSF` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `City` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PostalCode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Telephone1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Telephone2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mobile` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Telefax` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Function` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LastUpdateDate` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Status` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Country`,`CountryAbbr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
