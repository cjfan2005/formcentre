var StringUtils = {
	isBlank: function(obj) {
		if(null==obj) return true;
		if(obj.value==null) return true;
		if(obj.value=='') return true;
		if(obj.value.length==0) return true;
		if(StringUtils.trim(obj.value).length==0) return true;
		if(obj.value=='null') return true;
		return false;
	},
	isNotBlank: function(obj) {
		if(StringUtils.isBlank(obj)) return false;
		return true;
	},
	isTxtBlank: function(txt) {
		if(null==txt) return true;
		if(txt=='') return true;
		if(txt.length==0) return true;
		if(StringUtils.trim(txt).length==0) return true;
		if(txt=='null') return true;
		return false;
	},
	isTxtNotBlank: function(txt) {
		if(StringUtils.isTxtBlank(txt)) return false;
		return true;
	},
	leftPad: function(obj, leng, word) {
		var padding = "";
		for(var i=0; i<leng; i++) padding += word;
		return( padding.substr(0, (padding.length-obj.value.length) )+obj.value );
	},
	trim: function(txt) {
		return txt.replace(/(^\s*)|(\s*$)/g, ""); 
	},
	ltrim: function(txt) {
		return txt.replace(/(^\s*)/g, ""); 
	},
	rtrim: function(txt) { 
		return txt.replace(/(\s*$)/g, ""); 
	} 
};