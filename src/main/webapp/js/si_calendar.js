function SiCalendar(winObj){
    this.windowObj = winObj; 
    this.targetObj;
    this.calendarLocation;
    
    this.getUSDate = getUSDate;
    this.getCHDate = getCHDate;
    this.setTargetObj = setTargetObj;
    this.setWindowName = setWindowName;
    this.setWindowStatus = setWindowStatus;    
    this.setCalendarLocation = setCalendarLocation; 
    
    
    this._openWindowName = "_calendar_";
    this._openWindowStatus = "toolbar=no,scrollbar=no,location=0,resizable=0,directories=no,width=200,height=180,top=300,left=300";
    
    function setCalendarLocation(pathStr){
    	this.calendarLocation = pathStr;
    }
    
    function getUSDate(tObj){
    	if(tObj.type != "text"){
    		this.targetObj = this.windowObj.document.all[tObj];
    	}else{
    	    this.targetObj = tObj;
    	}    	    	
    	this.windowObj.open(this.calendarLocation+"../calendar/calendar.htm?targetObj="+this.targetObj.name,this._openWindowName,this._openWindowStatus);
    }
    
    function getCHDate(tObj){
    	if(tObj.type != "text"){
    		this.targetObj = this.windowObj.document.all[tObj];
    	}else{
    	    this.targetObj = tObj;
    	}
    	this.windowObj.open(this.calendarLocation+"../calendar/calendar2.htm?targetObj="+this.targetObj.name,this._openWindowName,this._openWindowStatus);
    }
    
    function setTargetObj(returnDate){
    	this.targetObj.value = returnDate;
    }
    
    function setWindowName(setStr){
    	this._openWindowName = setStr;
    }
    
    function setWindowStatus(setStr){
    	this._openWindowStatus = setStr;
    }
        
}