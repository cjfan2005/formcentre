<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.atos.audit.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Form Centre</title>

<script type="text/javascript">	
	$(window).resize(function() {
		$("#grid").jqGrid('setGridWidth', $(window).width() - 200);
	});
	var selectedId="";
	$(document).ready(function() {
		
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/auditLog/SearchAuditLogAction.do?date=${date}&dateRange=${dateRange}&userId=${userId}&auditType=${auditType}'),
			//url:'${pageContext.request.contextPath}/auditLog/InitAuditLogJSONAction.do?date=${date}&dateRange=${dateRange}&userId=${userId}&auditType=${auditType}',
			//url:'${pageContext.request.contextPath}/auditLog/SearchAuditLogAction.do',
			datatype: "json",
			height: "100%",  // Grid高度
			width:$(window).width() - 200,
			colNames:['使用者', '時間', '程式名稱','類型','LOG'],
	        colModel:[
					  {name:'UserId',width:35,sortable:true},
					  {name:'AuditTime',index:'AuditTime', align:'right',width:80, sortable:true},
					  {name:'ProgramName',width:120, sortable:false},
					  {name:'AuditType',width:20, sortable:false},
					  {name:'SqlLog', align:'right',width:300, sortable:false}
	        ],
	        rownumbers: true,
	        sortable:true,
	        sortname:'AuditTime',
	        sortorder:'desc',  
	        viewrecords: true,
	        rowNum:20, 
	        rowList:[20,40,80,100],
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records"}, 
	        //caption: "Log查詢",
            pager:"#gridPager"
			/* gridComplete: function() { //onload時做的事情
				var $self = $(this);
				if($self.jqGrid("getGridParam","datatype") == "json"){
					setTimeout(function(){
						$self.trigger("relaodGird");
					},50);
				}
			} */
	        
	    });
	});

	function searchAudit(){
		
		var date = $('#date').val();
		var userId = $('#userId').val();
		var auditType = $('#auditType').val();
		var dateRange = $('#dateRange').val();
		
		
		var urlPatten = "${pageContext.request.contextPath}/auditLog/SearchAuditLogAction.do?date=" + date + "&dateRange=" + dateRange + "&userId=" + userId + "&auditType=" + auditType;
		
		jQuery("#grid").jqGrid('setGridParam', {url:urlPatten});
		jQuery("#grid").trigger('reloadGrid');
	}
	
</script>
</head>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/tpe_logo.png" alt="" border="0" /></a></div>
<%@include file="/menus_general.jsp" %>
<br><br><br><br>
<div class="path">
PATH： 
AuditLog :
AuditLog查詢</div>
<div class="body">
<div class="body">
<div class="dialog">

<s:actionerror/><s:actionmessage/>

<!-- Print out Search Condition on page -->
<c:if test="${(userId!='' && userId!=undefined && userId!=null)}">
		<div class="queryConditionMsg">
		   [查 詢 條 件]<br>
		   使用者帳號：<c:out value='${userId}' default='null'/><br>
		</div>
</c:if>

<form ACTION="" METHOD="POST" name="form1" id="form1" enctype="multipart/form-data">
<table width="900">
	  
  <tr>
    <th>UserID：</th>
    <td width="50" >
      <input name="userId" id="userId"  size="25" maxlength="50" lang="en" />
    </td>
    <th>類別：</th>
    <td width="50" >
       <select name="auditType" id="auditType">
			<option value="0">登入</option>
			<option value="1">登出</option>
			<option value="2">新增</option>
			<option value="3">修改</option>
			<option value="4">刪除</option>
		</select>
    </td>
    <td align="center">
    <th>日期區間：</th>
    <td width="50">
         <input type="hidden" id="date" name="date" />
		<select name="dateRange" id="dateRange">
			<option value="1">一個月</option>
			<option value="2">兩個月</option>
			<option value="3">三個月</option>
		</select>
    </td>
    <td align="left">
    <s:submit onclick="searchAudit()" cssClass="comfirm" value="查詢" theme="simple"/>
    
    </td>
  </tr>
</table>	
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>

</form>

</div>
</div>
</div>
</body>
</html>
 