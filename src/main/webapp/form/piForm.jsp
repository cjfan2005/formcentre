<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.atos.piForm.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/IncludePage.jsp"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jqGrid/themes/redmond/jquery-ui-custom.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Form Management - PI Form</title>

<style>
label.error{display:inline;margin-left: 10px; color: red;}
TD{padding-left:10px;} 
#wrap { display:table; }
#cell { display:table-cell; vertical-align:middle; }
</style>
<script type="text/javascript">
	function submitForm() {
	    if($("#form1").valid()){
	    	$( "#dialog" ).dialog( "open" );
	    }else {
	    	$( "#warn" ).dialog( "open" );
	    }
	}
	
	function submitFunc() {
		var c = document.getElementById("country").value;
		var ca = document.getElementById("countryAbbr").value;
	    var saveOrUptdateConditions = c+";"+ca
		$('#form1').attr('action',
				'${pageContext.request.contextPath}/form/PiFormSubmitAction.do?saveOrUptdateConditions='+saveOrUptdateConditions);
		$('#form1').submit();
	}
	
	function genPDF() {
		var c = document.getElementById("country").value;
		var ca = document.getElementById("countryAbbr").value;
	    var PDFConditions = c+";"+ca
		$('#form1').attr('action',
				'${pageContext.request.contextPath}/form/GenPiformPDFAction.do?PDFConditions='+PDFConditions);
		$('#form1').submit();
	}
	
	$(function(){
		//須與form表單ID名稱相同
		$("#form1").validate({});
		$( "#warn" ).dialog({
			"autoOpen": false,
			buttons: [
			          {
			            text: "OK",
			            click: function() {
			            	$( this ).dialog( "close" );
			            }
			          }]
		});
		
		$( "#dialog" ).dialog({
			"autoOpen": false,
			width: 250,
			height:200,
			buttons: [
			          {
			            text: "OK",
			            click: function() {
			            	submitFunc();
			            }
			          },
			          {
				        text: "Cancel",
				        click: function() {
				           $( this ).dialog( "close" );
				        }
				      }
			        ]	
		
		});
		init();
	});
	
	function init() {
		var CountryObj = document.getElementById("country");
		CountryObj.style.display = "none";
		var tarObj = document.getElementById("country");
		var selectObj = document.createElement("SELECT");
		<c:if test="${status == 'Approved'}">
			selectObj.disabled = "disabled";
		</c:if>
		
		insertAfter(selectObj, tarObj)
		$.ajax({
  			type: 'POST',
  			url: "${pageContext.request.contextPath}/form/GetCountryMapJSONAction.do",
  			dataType: "json",
  			success: function(data) {
  				var sValue = CountryObj.value;
  				var index = 0;
  				for(var i=0;i<data.countryMapJsonStr.length;i++) {
  					var optionObj = document.createElement("OPTION");
  					if(sValue === data.countryMapJsonStr[i].country) {
  						index = i;
  					}
  					optionObj.innerHTML = data.countryMapJsonStr[i].country;
  					optionObj.value = data.countryMapJsonStr[i].countryCode +"@"+ data.countryMapJsonStr[i].countryAbbr;
  					selectObj.appendChild(optionObj);
  				}
  				selectObj.selectedIndex = index;
  			}
		});
		selectObj.onchange = function() {
			var both = this.value;
			var arr = this.value.split("@");
			$("#countryCode").val( arr[0] );
			$("#lblCountryCode").html( arr[0] );
			$("#countryAbbr").val( arr[1] );
			$("#lblCountryAbbr").html( arr[1] );
			$("#country").val( this.options[this.selectedIndex].text );
		};
	}
	function insertAfter(newEl, targetEl) {
        var parentEl = targetEl.parentNode;
        if(parentEl.lastChild == targetEl) {
            parentEl.appendChild(newEl);
        }else {
            parentEl.insertBefore(newEl,targetEl.nextSibling);
        }            
    }
</script>

</head>
<body>
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/universiade_logo.png" alt="" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>

<br><br><br>
<div class="path">
PATH： PI Form :
Fill up PI Form</div>
<div class="body">
<div class="body">

<div class="dialog">
<form action="" method="post" name="form1" id="form1">
<table width="1000">
<s:hidden id="isNewData" name="isNewData" value="%{isNewData}"/>
	<tr>
	<th>Country：</th><td>
	<s:textfield theme="simple" name="country" id="country" value="%{country}" ReadOnly="true" style="display:none;"/>
	</td>
	<th>CountryAbbr：</th><td><label id="lblCountryAbbr" style="display:inline;color:#708090;"><s:property value="%{countryAbbr}" /></label><s:textfield theme="simple" name="countryAbbr" id="countryAbbr" value="%{countryAbbr}" ReadOnly="true" style="display:none;"/></td>
	</tr>
	<tr>
		<th>Name of the NUSF：</th>
		<td colspan="3"><s:textfield theme="simple" name="nameOfTheNUSF" id="nameOfTheNUSF" type="text" size="100" maxlength="255" value="%{nameOfTheNUSF}" cssClass="required form-control" /></td>
	</tr>
	<tr><th>Address：</th><td colspan="3"><s:textfield theme="simple" name="address" id="address" type="text" size="120" maxlength="255" value="%{address}" cssClass="required" /></td></tr>
	<tr>
		<th>City：</th><td><s:textfield theme="simple" name="city" id="city" type="text" size="20" maxlength="50" value="%{city}" cssClass="required" /></td>
		<th>Postal Code：</th><td><s:textfield theme="simple" name="postalCode" id="postalCode" type="text" size="20" maxlength="20" value="%{postalCode}" cssClass="required number  form-control" /></td>
	</tr>
	<tr>
		<th>Telephone1：</th><td><label id="lblCountryCode" style="display:inline;color:#708090;margin-right:10px;"><s:property value="%{countryCode}" /></label><s:textfield theme="simple" name="telephone1" id="telephone1" type="text" size="20" maxlength="50" value="%{telephone1}" cssClass="required number" />
		<s:textfield theme="simple" name="countryCode" id="countryCode" value="%{countryCode}" readonly="true" style="display:none;"/>
		</td>
		<th>Telephone2：</th><td style="margin-right:20px;"><s:textfield theme="simple" name="telephone2" id="telephone2" type="text" size="20" maxlength="50" value="%{telephone2}" cssClass="required number" /></td>
	</tr>
	<tr>
		<th>Mobile：</th><td><s:textfield theme="simple" name="mobile" id="mobile" type="text" size="20" maxlength="50" value="%{mobile}" cssClass="required number" /></td>
		<th>Telefax：</th><td><s:textfield theme="simple" name="telefax" id="telefax" type="text" size="20" maxlength="50" value="%{telefax}" cssClass="required number" /></td>
	</tr>
	<tr>
		<th>E-mail：</th><td colspan="3"><s:textfield theme="simple" name="email" id="email" type="text" size="50" maxlength="200" value="%{email}" cssClass="required email" /></td>
	</tr>
	<tr>
		<c:if test="${isNewData == 'NO'}">
		<th>Name：</th><td><s:textfield theme="simple" name="name" id="name" type="text" size="20" maxlength="255" value="%{name}" cssClass="required" /></td>
		<th>Date：</th><td><s:property value="%{dateStr}" /></td>
		</c:if>
		<c:if test="${isNewData == 'YES'}">
		<th>Name：</th><td colspan="3"><s:textfield theme="simple" name="name" id="name" type="text" size="20" maxlength="255" value="%{name}" cssClass="required" /></td>
		</c:if>
	</tr>
	<c:if test="${isNewData == 'NO'}"> 
     <tr>
       <th>Status：</th>
       <td colspan="3">
         <s:textfield theme="simple" name="status" id="status" value="%{status}" ReadOnly="true" style="display:none;"/>
         <s:property value="%{status}" />
       </td>
     </tr>
  </c:if>
	<tr>
	<td colspan="4">
		<c:choose>
		
	     <c:when test="${isNewData == 'YES'}">
  				<input type="button" value="Submit Form" onclick="submitForm();" class="comfirm" />
	     </c:when>
	     <c:when test="${status == 'Approved'}">
  				<input type="button" value=".::Download PDF::." onclick="genPDF();" class="buttonBlue" />
	     </c:when>
	     <c:otherwise>
   				<input type="button" value=".::Update Form::." onclick="submitForm();" class="comfirm" />
   				<input type="button" value=".::Download PDF::." onclick="genPDF();" class="buttonBlue" />
         </c:otherwise>
	   </c:choose>
	   </td>
	</tr>
</table>
<!--  
  <tr>
    <th>Fuction：</th>
    <td>
      <s:textfield name="function" id="funtion" type="text" size="20" maxlength="255" value="%{function}" />
    </td>
  </tr>  
-->
</form>

<div id="dialog"title="Confirm">
	<DIV style="width:200px;height:70px;text-align:center;line-height:100px;">Save or not?</DIV>
</div>
<div id="warn" title="Warn">
	<DIV style="width:260px;height:70px;text-align:center;line-height:100px;">Please check every column !</DIV>
</div>

</div>
</div>
</div>
</body>
</html>
 