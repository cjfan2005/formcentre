<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.atos.user.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.ui.autocomplete.min.js"></script>

<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Form Management- Manage Form </title>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 175px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 1px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:64px;
        color: royalblue;
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		
		$("#grid").jqGrid({
			
			url:encodeURI('${pageContext.request.contextPath}/form/PiFormManageAction.do?country=${country}&countryAbbr=${countryAbbr}&status=${status}'),
			
			datatype: "JSON",
			height: "350", 
			width:"900",
			colNames:['Country [CountryAbbr.]', 'Name of the NUSF',
			          'Status', 'Download', 'Last Update', 'Country', 'CoAbbr', 'CountryCode',
			          'Address', 'City', 'PostalCode', 'Telephone1', 'Telephone2', 'Mobile', 'Telfax',
			          'Email', 'Name', 'Function'],
	        colModel:[
					  {name:'C&CA', index:'C&CA', width:200, align:'right', frozen:false}, //Country & CountryCode
					  
	                  {name:'nameOfTheNUSF', index:'nameOfTheNUSF', width:200, frozen:false,
						  editable: true, editoptions:{size:100, maxlength: 200},
	                	  edittype:"text", editrules:{required:true}  
					  },
					  
					  {name:'status', index:'status', width:80, align:'center',
	                	  editable: true, editoptions:{size:10, maxlength: 10},
	                	  edittype:'select', 
	                	  editoptions:{value:{1:'Reviewing', 2:'Approved'}},
	                	  editrules:{required:true}, hidden:false,
	                  },
	                  
	                  {name:'btn',index:'btn',sortable:false, width:60, align:'center',
		                 formatter:function (cellvalue, options, rowObject) {
		                    return '<input class="buttonBlue" type="button" onClick="genPDF(\''+ rowObject.country +';'+ rowObject.countryAbbr +'\')" type="button" value="PDF"/>'
		                 }
		              },
		              
	                  {name:'lastUpdateDate', index:'lastUpdateDate', width:150, 
	                	  align:'center',
	                	  editable: true, editoptions:{size:100, maxlength: 200},
	                	  edittype:"text", editrules:{required:true}, hidden:true
	                  },
	                  
	                  {name:'country', index:'country', width:160, 
						  editable: true, editoptions:{size:35, maxlength: 10},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'countryAbbr', index:'countryAbbr', width:150,
						  editable: true, editoptions:{size:10, maxlength: 10},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'countryCode', index:'countryCode', width:150,
						  editable: true, editoptions:{size:10, maxlength: 10},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
	                  
					  {name:'address', index:'address', width:360,
						  editable: true, editoptions:{size:100, maxlength: 200},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'city', index:'city', width:150,
						  editable: true, editoptions:{size:50, maxlength: 50},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'postalCode', index:'postalCode', width:150,
						  editable: true, editoptions:{size:10, maxlength: 20},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'telephone1', index:'telephone1', width:150,
						  editable: true, editoptions:{size:20, maxlength: 50},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'telephone2', index:'telephone2', width:150,
						  editable: true, editoptions:{size:20, maxlength:50},
	                	  edittype:"text", editrules:{required:false}, hidden:false
					  },
					  {name:'mobile', index:'mobile', width:150,
						  editable: true, editoptions:{size:50, maxlength: 50},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'telefax', index:'telefax', width:150,
						  editable: true, editoptions:{size:50, maxlength:50},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'email', index:'email', width:150,
						  editable: true, editoptions:{size:50, maxlength: 200},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'name', index:'name', width:150,
						  editable: true, editoptions:{size:20, maxlength: 200},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'function', index:'function', width:150,
						  editable: true, editoptions:{size:20, maxlength: 200},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  }
	        ],
	        shrinkToFit:false,
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 10, 
	        rowList: [10,20], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "PI Form Management",
	        toppager: true,
	        loadComplete: function(data){
	        	$("#ulList").css("zIndex", 1000);
	        }
	    });
		
		jQuery("#grid").jqGrid('navGrid','#gridPager', 
			{edit:true, add:false, del:false, search:false, refresh:false, cloneToTop:true,
			     edittext: "Detail, Modify Or Approve", edittitle: "Detail, Modify Or Approve", 
			     addtext: "Add", addtitle: "Add", 
			     deltext: "Delete", deltitle: "Delete",
			     searchtext: "Search", searchtitle: "Search", 
			     refreshtext: "Refresh", refreshtitle: "Refresh"
			},
			
			// Modify Or Approve
			{ url: '${pageContext.request.contextPath}/form/PiFormSubmitAction.do',
			  height:'78%',
			  width:'100%',
			  //viewPagerButtons: false, //remove previous/next arrow
              reloadAfterSubmit: true,
              onclickSubmit : function(params, posdata) { 
               	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
               	  $("#allCover").css("display","block");
              },
              afterSubmit : function(response, postdata) {
            	  $("#allCover").css("display","none");
              	  return true;
              },
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','country',{editoptions:{readonly:'readonly'}});
            	  $("#grid").jqGrid('setColProp','countryAbbr',{editoptions:{readonly:'readonly'}});
            	  $("#grid").jqGrid('setColProp','countryCode',{editoptions:{readonly:'readonly'}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/user/ModifyUserAction.do?modifyUserAct=add',
			  height:320,  
			  width:500,	
		      reloadAfterSubmit:true,
		      closeAfterAdd: true,
		      recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','userId',{editoptions:{readonly:false}});
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/user/ModifyUserAction.do?modifyUserAct=delete',
			  
			  delData: {
				  userId: function() {
				      var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
                      var value = $("#grid").jqGrid('getCell', selectedId, 'userId');
                      //alert(value);
                      return value;
			      }
			  },
			  reloadAfterSubmit: false
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/user/JqGridFindAllStaff.do?UserType=STAFF'
			} 
		)
		jQuery("#grid").jqGrid('setFrozenColumns');
		
	});

	function genPDF(PDFConditions) {
		//alert(PDFConditions);
		$('#form1').attr('action',
				'${pageContext.request.contextPath}/form/GenPiformPDFAction.do?PDFConditions='+PDFConditions);
		$('#form1').submit();
		$('#form1').attr('action',"");
	}
	
</script>

</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/universiade_logo.png" alt="Universiade" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： Form Management :
PIForm List</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>

<!-- Print out Search Condition on page 
<c:if test="${(userId!='' && userId!=undefined && userId!=null)}">
		<div class="queryConditionMsg">
		   [QueryCondition(s)]<br>
		   Account：<c:out value='${userId}' default='null'/><br>
		</div>
</c:if>
-->

<form action="" method="post" name="form1" id="form1">

<table width="400" >
  <tr>
    
    <th>Status：</th>
    <td width="50">
      <select name="status" id="status">
         <option value="">ALL</option>
		 <option value="1">Reviewing</option>
		 <option value="2">Approved</option>
      </select>
    </td>
    <th>Country：</th>
    <td width="50">
      <input name="country" id="country" type="text" size="20" maxlength="20" value=""  lang="en" />
    </td>
    <th>Country Abbreviation：</th>
    <td width="50">
      <input name="countryAbbr" id="countryAbbr" type="text" size="20" maxlength="20" value=""  lang="en" />
    </td>
    <td>
    <s:submit onclick="searchForm()" cssClass="comfirm" value="Query" theme="simple"/>
    </td>
  </tr>
</table>
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
<!--   <table id="testTable">
	 <td>
	   選擇使用者帳號(User ID) : <span id="showUserId"></span>
	 </td>
  </table> -->
</form>
</div>
</div>
</div>
</body>
</html>
 