
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jqGrid/css/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jqGrid/themes/redmond/jquery-ui-custom.css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/jqGrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jqGrid/js/i18n/grid.locale-en.js"></script>
<script src="${pageContext.request.contextPath}/jqGrid/js/jquery.layout.js" type="text/javascript"></script>

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>


<script src="${pageContext.request.contextPath}/jqGrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/jqGrid/plugins/jquery.tablednd.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/jqGrid/plugins/jquery.contextmenu.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/ajaxfileupload.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.jqprint-0.3.js" type="text/javascript" ></script>
