package net.atos.user.bean;

/**
 * 存放登入者資訊
 * 
 * @author KENT
 * @since 2016/01/06
 */
public class LoginInfo {

	private String userId;
	private String clientIpAddr;
	private String password;

	public LoginInfo() {
		super();
	}

	public LoginInfo(String userId, String clientIpAddr, String password) {
		super();
		this.userId = userId;
		this.clientIpAddr = clientIpAddr;
		this.password = password;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getClientIpAddr() {
		return clientIpAddr;
	}

	public void setClientIpAddr(String clientIpAddr) {
		this.clientIpAddr = clientIpAddr;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}