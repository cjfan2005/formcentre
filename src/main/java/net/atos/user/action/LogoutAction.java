package net.atos.user.action;

import java.util.ResourceBundle;

import org.apache.struts2.ServletActionContext;
import net.atos.interceptor.BaseAction;

public class LogoutAction extends BaseAction {
	ResourceBundle rbMsg = ResourceBundle.getBundle(SYS_MSG_Control);
	// all struts logic from here
	public String execute() {

		ServletActionContext.getRequest().getSession().invalidate();
		addActionMessage(rbMsg.getString("LogoutSuccess"));
		return "logout";

	}
}
