package net.atos.user.action;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import net.atos.interceptor.BaseAction;
import net.atos.user.bean.LoginInfo;
import net.atos.user.dao.LoginDAO;

public class LoginAction extends BaseAction {
	ResourceBundle rbMsg = ResourceBundle.getBundle(SYS_MSG_Control);
	@Autowired
	private LoginDAO loginDAO;
	private String userId;
	private String password;
	private String role;
	public String loginResult = null;
	LoginInfo loginInfo = null;

	// all struts logic from here
	public String execute() {
		
		String ip = "";
		ServletActionContext.getRequest().getSession().setAttribute("loggedInUser", userId);
		ip = ServletActionContext.getRequest().getHeader("X-FORWARDED-FOR");
        //ip = getRequest().getHeader("x-forwarded-for".toLowerCase());
		
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = ServletActionContext.getRequest().getHeader("Proxy-Client-IP".toLowerCase());  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = ServletActionContext.getRequest().getHeader("WL-Proxy-Client-IP".toLowerCase()); 
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = ServletActionContext.getRequest().getHeader("HTTP_CLIENT_IP".toLowerCase());  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = ServletActionContext.getRequest().getHeader("HTTP_X_FORWARDED_FOR".toLowerCase());  
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = ServletActionContext.getRequest().getRemoteAddr();
        }
        
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			try {
				ip = InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}

        if(ip != null && ip.contains("%")){
			ip = ip.replace("%", "");
		}
		
        //記錄登入者IP
        ServletActionContext.getRequest().getSession().setAttribute("loginIp", ip);
        ServletActionContext.getRequest().getSession().setAttribute("loginPwd", password);
        loginInfo = getLoginInfo();
        
		//loginResult = loginDAO.validateUser(userId, password);
        loginResult = loginDAO.validateUser(loginInfo);

		if (loginResult.isEmpty() || loginResult.equals("input")) {
			addActionError(rbMsg.getString("UserIdOrPwdIncorrect"));
			return "input";
		} else if (loginResult.equals("admin-login-success"))
			return "admin-login-success";

		return "login-success";//general user login success;
	}

	// simple validation
	public void validate() {
		if (userId.trim().equalsIgnoreCase("") || password.trim().equalsIgnoreCase("")) {
			addActionError(rbMsg.getString("UserIdAndPwdBlank"));
		} else {
			addActionMessage("You are valid user!");
		}
	}
	
	public LoginDAO getLoginDAO() {
		return loginDAO;
	}

	public void setLoginDAO(LoginDAO loginDAO) {
		this.loginDAO = loginDAO;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
}