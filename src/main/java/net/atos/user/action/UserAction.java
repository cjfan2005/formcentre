package net.atos.user.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsStatics;
import org.springframework.beans.factory.annotation.Autowired;
import com.opensymphony.xwork2.ognl.OgnlValueStack;

import net.atos.user.dao.UserDAO;
import net.atos.util.PageParameter;
import net.atos.interceptor.BaseAction;
import net.atos.user.bean.Users;

public class UserAction extends BaseAction {
	ResourceBundle rbMsg = ResourceBundle.getBundle(SYS_MSG_Control);
	private String userId, name, role, mobile, email, password, deleteFlag, creater, modifyUserAct, notifyGroup, note;
	private String country, countryAbbr; //Country and CountryAbbr.
	private Date createdate = null;
	@Autowired
	private UserDAO userDAO;

	// for QUERY
	private String createdateBgnStr, createdateEndStr;
	private Date createdateBgn, createdateEnd;
	private Users ui;
	private String url;

	// 接收前端參數
	private List<Map<String, Object>> rows;
	private Map result;

	// jqgrid
	private int total = 0; // 總頁數
	private int records = 0; // 資料總筆數
	private int pageSize = 0;
	private int currentPageNo = 0;
	private String sort;
	private String orderBy;

	public String getUrl() {
		return url;
	}

	public String ListAllUser() {
		deleteFlag = "N";
		return "success";
	}
	
	public String ListAllDeletedUser() {
		deleteFlag = "Y";
		return "success";
	}

	public String SaveOrUpdateOrDeleteUser() {
		OgnlValueStack stack = (OgnlValueStack) requestMap.get("struts.valueStack");
		HttpServletRequest request = (HttpServletRequest) stack.getContext().get(StrutsStatics.HTTP_REQUEST);

		modifyUserAct = request.getParameter("modifyUserAct");
		Users u = new Users();
		
		//取得目前登入使用者角色
		String loggedInRole = ServletActionContext.getRequest().getSession().getAttribute("loggedInRole").toString();
		if(!loggedInRole.equalsIgnoreCase("ADMIN")) {
			addActionError(rbMsg.getString("authFail")); //無權限，提前return出去。
			addActionMessage(rbMsg.getString("authFail"));
			return "input";
		}
		
		if (modifyUserAct.equalsIgnoreCase("add")) {
			u.setUserId(userId);
			u.setPassword(password);
			u.setName(name);
			u.setRole(Integer.parseInt(role));
			u.setMobile(mobile);
			u.setEmail(email);
			u.setDeleteFlag("N");
			u.setNotifyGroup(Integer.parseInt(notifyGroup));
			u.setCreateDate(new Date());
			u.setNote(note);
			try {
				userDAO.modifyUser(u, "add");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
		} else if (modifyUserAct.equalsIgnoreCase("modify")) {
			u.setUserId(userId);
			u.setPassword(password);
			u.setName(name);
			u.setMobile(mobile);//=tel
			u.setEmail(email);
			u.setNote(note);

			try {
				userDAO.modifyUser(u, "modify");
				//url = "InitModifyUserAction.do?userId=" + userId;
			} catch (Exception e) {
				e.printStackTrace();
			}

			return "success";
		} else if (modifyUserAct.equalsIgnoreCase("delete")) {
			u.setUserId(userId);
			u.setNote(note);//Delete reason.
			try {
				userDAO.modifyUser(u, "delete");
			} catch (Exception e) {
				e.printStackTrace();
			}

			return "success";
		}  else if (modifyUserAct.equalsIgnoreCase("recovery")) { //deleteFlag from Y to N
			u.setUserId(userId);
			u.setRole(Integer.parseInt(role));
			u.setMobile(mobile);
			u.setEmail(email);
			u.setNotifyGroup(Integer.parseInt(notifyGroup));
			u.setNote(note);
			System.out.println("recovery - deleteFlag:" + deleteFlag);
			u.setDeleteFlag(deleteFlag);

			try {
				userDAO.modifyUser(u, "recovery");
			} catch (Exception e) {
				e.printStackTrace();
			}

			return "success";
		} else {
			return "input";
		}
	}


	@SuppressWarnings("unchecked")
	public String ListUserCondition() {
		
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);

		Users u = new Users();
		if ((userId != null && !userId.isEmpty())) {
			u.setUserId(userId);
		}
		if ((deleteFlag != null && !deleteFlag.isEmpty())) {
			u.setDeleteFlag(deleteFlag);
		}
		
		List<Map<String, Object>> objList = userDAO.getUserCondition(u, country, countryAbbr, sort, orderBy);
		int count = objList.size();

		objList = dataPagination(objList, page);

		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> usersList = new ArrayList<Map<String, Object>>();
		Map<String, Object> users;

		while (its.hasNext()) {
			users = new HashMap<String, Object>();

			Object[] obj = (Object[]) its.next();

			users.put("userId", obj[0]);
			users.put("password", obj[1]);
			users.put("name", obj[2]);
			users.put("email", obj[3]);
			users.put("mobile", obj[4]);//=tel
			users.put("deleteFlag", obj[5]);
			users.put("note", obj[6]);
			users.put("role", obj[7]);
			users.put("createDate", obj[8]);
			users.put("country", obj[9] + " [" + obj[10] + "]");
			//users.put("countryAbbr", obj[10]);
			users.put("countryCode", obj[11]);
			
			
			usersList.add(users);
		}

		rows = usersList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);

		return SUCCESS;
	}

	//for Kent use only.
	public void genUserAccount() {
		userDAO.genUserAccount();
		System.out.println("gen accounts successfully.");
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getCreatedateBgnStr() {
		return createdateBgnStr;
	}

	public void setCreatedateBgnStr(String createdateBgnStr) {
		this.createdateBgnStr = createdateBgnStr;
	}

	public String getCreatedateEndStr() {
		return createdateEndStr;
	}

	public void setCreatedateEndStr(String createdateEndStr) {
		this.createdateEndStr = createdateEndStr;
	}

	public Date getCreatedateBgn() {
		return createdateBgn;
	}

	public void setCreatedateBgn(Date createdateBgn) {
		this.createdateBgn = createdateBgn;
	}

	public Date getCreatedateEnd() {
		return createdateEnd;
	}

	public void setCreatedateEnd(Date createdateEnd) {
		this.createdateEnd = createdateEnd;
	}


	public String getModifyUserAct() {
		return modifyUserAct;
	}

	public void setModifyUserAct(String modifyUserAct) {
		this.modifyUserAct = modifyUserAct;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Users getUi() {
		return ui;
	}

	public void setUi(Users ui) {
		this.ui = ui;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public List<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public Map getResult() {
		return result;
	}

	public void setResult(Map result) {
		this.result = result;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getNotifyGroup() {
		return notifyGroup;
	}

	public void setNotifyGroup(String notifyGroup) {
		this.notifyGroup = notifyGroup;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryAbbr() {
		return countryAbbr;
	}

	public void setCountryAbbr(String countryAbbr) {
		this.countryAbbr = countryAbbr;
	}

}
