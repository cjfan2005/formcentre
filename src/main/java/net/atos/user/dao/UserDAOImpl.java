package net.atos.user.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import net.atos.piForm.bean.CountryMap;
import net.atos.user.bean.Users;
import net.atos.util.StringUtils;
import tipo.SI.DataEncrypt;

public class UserDAOImpl implements UserDAO {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public List<Users> getAllUser(String deleteFlag) {
		Session session = null;
		session = sessionFactory.openSession();
		
		Criteria c = session.createCriteria(Users.class);
		if (deleteFlag != null && !deleteFlag.isEmpty())
			c.add(Restrictions.eq("deleteFlag", deleteFlag));
		List<Users> uList = c.list();
		session.close();
		return uList;
	}
	
	/**
	 * 點選『DELETE』,deleteFlag改為"Y".
	 */
	public String deleteUser(String userId) {
		Session session = null;
		String sql = "UPDATE USERS SET deleteFlag='Y' WHERE userId=" + userId + "";
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.createSQLQuery(sql).executeUpdate();
			t.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getUserCondition(Users u, String country, String countryAbbr, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			sql += " SELECT ";
			sql += " u.userId, u.password, u.name, u.email, u.mobile, u.deleteFlag, u.note, ";
			sql += " cm.Note1,"; //role
			sql += " u.createDate, co.Country, co.countryAbbr, co.CountryCode ";
			sql += " FROM USERS AS u ";
			sql += " LEFT JOIN CountryMap AS co  ON co.CountryAbbr = u.countryAbbr ";
			sql += " LEFT JOIN CodeMap AS cm  ON cm.CodeSequence = u.role ";
			
			sql += " WHERE 1=1 ";
			
			if(StringUtils.isNotBlank(u.getUserId()))
				sql += " AND u.userId like '%" + u.getUserId() + "%' ";
			
			if(StringUtils.isNotBlank(country))
				sql += " AND co.Country like '%" + country + "%' ";
			
			if(StringUtils.isNotBlank(countryAbbr))
				sql += " AND co.CountryAbbr like '%" + countryAbbr + "%' ";
			
			if(StringUtils.isNotBlank(u.getDeleteFlag()))
				sql += " AND u.deleteFlag = '" + u.getDeleteFlag() + "' ";
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy;
			}else{
				sql += " ORDER BY ";
				sql += " u.userId ASC ";
			}
			
			Query query = session.createSQLQuery(sql);
			list = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	

	public List<Users> getUserCondition(Users u, Date createdateBgn, Date createdateEnd){
		Session session = null;
		session = sessionFactory.openSession();
		Criteria c = session.createCriteria(Users.class);
		
		String userId = u.getUserId();
		String deleteFlag = u.getDeleteFlag();
		
		if (userId != null && !userId.isEmpty())
			c.add(Restrictions.eq("userId", userId));
		if (deleteFlag != null && !deleteFlag.isEmpty())
			c.add(Restrictions.eq("deleteFlag", deleteFlag));
		if (createdateBgn != null && createdateEnd != null)
		    c.add(Restrictions.between("createdate", createdateBgn, createdateEnd));
		
		List<Users> uList = c.list();
		session.close();
		return uList;
	}
	
	
	public String modifyUser(Users user, String modifyUserAct) {
    	Session session = null;
    	try {
    		session = sessionFactory.openSession();
    		Transaction t = session.beginTransaction();
    		String strSql = "";
    		if(modifyUserAct.equalsIgnoreCase("add") && user.getUserId().equalsIgnoreCase("admin")) {

    			session.save(user);
    			t.commit();
    			return "success";
    			
    		} else if(modifyUserAct.equalsIgnoreCase("modify")) {
    			strSql = " UPDATE USERS " +
    					" SET password= '"+ user.getPassword() +"' "+
    					" 	, name= '"+ user.getName() +"' "+
    					" 	, mobile = '"+ user.getMobile() +"' "+
    					" 	, email = '"+ user.getEmail() +"' "+
    					" 	, note= '"+ user.getNote() +"' "+
						" WHERE userId= '"+ user.getUserId() +"' ";
    			
    		} else if (modifyUserAct.equalsIgnoreCase("delete") && !user.getUserId().equalsIgnoreCase("admin")) {
    			strSql = "UPDATE USERS SET deleteFlag='Y' WHERE userId='" + user.getUserId() + "'";
    			
    		} else if (modifyUserAct.equalsIgnoreCase("recovery") && !user.getUserId().equalsIgnoreCase("admin")) {
    			strSql = " UPDATE USERS " +
    					" SET deleteFlag= '"+ user.getDeleteFlag() +"' "+
						" WHERE userId= '"+ user.getUserId() +"' ";
    		} else {
    			
    			return "success";
    		}
	    	
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
	    	
	    	
    	} catch (Exception e) {
			e.printStackTrace();
		} finally{
    		if (session.isOpen()) {
        		session.close();
        	} 
        }
    	return "success";
	}
	
    //temp. use only 
	public void genUserAccount() {
		
		/** 
		 Rule:
		  get necessary from CountryMap
		  Account Name: CountryAbbr + Serial number.
		  Account Pwd: CounryAbbr + CountryCode + FISU_member(Y/N)
		  
		  1. get all countries
		  2. by using the rules, gen. AccountName & Pwd one by one.
		  3. insert into table: USERS.
		 */
		
		List<Map<String, Object>> countryList = getCountries();
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		try {
			for(int i=0; i<countryList.size(); i++) {
				String countryAbbr = countryList.get(i).get("countryAbbr").toString();
			    String countryCode = countryList.get(i).get("countryCode").toString();
			    String fisuMember = countryList.get(i).get("fisuMember").toString();
				String userId = countryAbbr + "01";
				String password = countryAbbr.toLowerCase() + countryCode + fisuMember;
				
				Users userBean = null;
				DataEncrypt dataEncrypt = null;
				String encryptPwd = null;
				
				userBean = new Users();
				dataEncrypt = new DataEncrypt();
				encryptPwd = dataEncrypt.encrypt(password);
				
				userBean.setUserId(userId);
				userBean.setPassword(encryptPwd);
				userBean.setRole(20);
				userBean.setCreateDate(new Date());
				userBean.setDeleteFlag("N");
				userBean.setCountryAbbr(countryAbbr);
				session.saveOrUpdate(userBean);
			}
			t.commit();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			t.rollback();
			e1.printStackTrace();
		} finally {
			if(session.isOpen())
				session.close();
		}
		return;
	}
	
	
	private List<Map<String, Object>> getCountries() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> countryList = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			sql += "SELECT CountryAbbr, CountryCode, FisuMember FROM CountryMap Order by Country ASC;";
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
			
			Map<String, Object> countryMap;
			
			if(list.size() > 0) {
				//arrange
				Iterator<?> its = list.iterator();
				
				while (its.hasNext()) {
					countryMap = new HashMap<String, Object>();
					Object[] obj = (Object[]) its.next();
					countryMap.put("countryAbbr", obj[0].toString());
					countryMap.put("countryCode", obj[1].toString());
					countryMap.put("fisuMember", obj[2].toString());
					
					countryList.add(countryMap);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return countryList;
	}
}
