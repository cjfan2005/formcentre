package net.atos.user.dao;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.atos.user.bean.*;

public interface UserDAO {
	public String modifyUser(Users user, String modifyUserAct);
	public List<Users> getAllUser(String deleteFlag);
	public List<Map<String, Object>> getUserCondition(Users u, String country, String countryAbbr, String sort, String orderBy);
	
	
	//
	public void genUserAccount();
}