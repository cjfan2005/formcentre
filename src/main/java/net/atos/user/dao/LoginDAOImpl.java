package net.atos.user.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import net.atos.audit.dao.AuditDAO;
import net.atos.util.*;
import net.atos.user.bean.Users;
import net.atos.user.bean.LoginInfo;
import tipo.SI.DataEncrypt;

public class LoginDAOImpl implements LoginDAO {
	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
	@Autowired
	private AuditDAO auditDao;
	
	public String validateUser(LoginInfo loginInfo){
		Session session = sessionFactory.openSession();
//		Transaction transaction = session.beginTransaction();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		String loginRole="";
		//Encrypt password
		DataEncrypt dataEncrypt = null;
		String decryptPwd = "";
		try {
			dataEncrypt = new DataEncrypt();
			decryptPwd = dataEncrypt.encrypt(loginInfo.getPassword());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String sql = "";
		try {
			sql += " SELECT ";
			sql += " u.userId, u.password, u.name, u.email, u.mobile, u.deleteFlag, u.note, ";
			sql += " cm.Note1,"; //role
			sql += " cm2.Note2,";  //notifyGroup
			sql += " u.createDate, ";
			sql += " u.role , ";
			sql += " cm2.Note3, "; //Raw of the NotifyGroup.
			sql += " co.Country, co.CountryAbbr, co.CountryCode ";
			sql += " FROM USERS AS u ";
			sql += " LEFT JOIN CodeMap AS cm  ON cm.CodeSequence = u.role ";
			sql += " LEFT JOIN CodeMap AS cm2 ON cm2.CodeSequence = u.notifyGroup ";
			sql += " LEFT JOIN CountryMap AS co ON co.CountryAbbr = u.CountryAbbr ";
			sql += " WHERE u.userId = '" + loginInfo.getUserId() + "' ";
			sql += " AND u.password = '" + decryptPwd + "' ";
			sql += " AND u.deleteFlag = 'N';";
			Query query = session.createSQLQuery(sql);
			list =  query.list();
			
			if ((list == null) || (list.size()==0)) {
            	ServletActionContext.getRequest().getSession().setAttribute("loggedInRole", "GUEST");
            	//auditDao.createAudit(session, AuditUtils.getAudit(loginInfo, AuditUtils.LOGIN, AuditType.Login,"validateUser", sql, "Login Fail"));
            	//transaction.commit();
            	return "input";
            	
			} else {
				//資料整理
				Iterator<?> its = list.iterator();

				List<Map<String, Object>> usersList = new ArrayList<Map<String, Object>>();
				Map<String, Object> users;

				while (its.hasNext()) {
					users = new HashMap<String, Object>();

					Object[] obj = (Object[]) its.next();

					users.put("userId", obj[0]);
					users.put("password", obj[1]);
					users.put("name", obj[2]);
					users.put("email", obj[3]);
					users.put("mobile", obj[4]);
					users.put("deleteFlag", obj[5]);
					users.put("note", obj[6]);
					users.put("role", obj[7]);
					loginRole = obj[7].toString();
					users.put("notifyGroup", obj[8]);
					users.put("createDate", obj[9]);
					users.put("rawRole", obj[10]);
					users.put("rawNotifyGroup", obj[11]);
					users.put("country", obj[12]);
					users.put("countryAbbr", obj[13]);
					users.put("countryCode", obj[14]);
					
					usersList.add(users);
				}
				
				//寫入Audit Log
				ServletActionContext.getRequest().getSession().setAttribute("loggedInRole", usersList.get(0).get("role"));
				ServletActionContext.getRequest().getSession().setAttribute("loggedCountry", usersList.get(0).get("country"));
				ServletActionContext.getRequest().getSession().setAttribute("loggedCountryAbbr", usersList.get(0).get("countryAbbr"));
				ServletActionContext.getRequest().getSession().setAttribute("loggedCountryCode", usersList.get(0).get("countryCode"));
				//ServletActionContext.getRequest().getSession().setAttribute("loggedInRawRole", usersList.get(0).get("rawRole"));
				
				//ServletActionContext.getRequest().getSession().setAttribute("loggedNotifyGroup", usersList.get(0).get("notifyGroup"));
				//ServletActionContext.getRequest().getSession().setAttribute("loggedRawNotifyGroup", usersList.get(0).get("rawNotifyGroup"));
	            //auditDao.createAudit(session, AuditUtils.getAudit(loginInfo, AuditUtils.LOGIN, AuditType.Login,"validateUser", sql, "Login Success"));
	            //transaction.commit();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()){
				session.close();
			}
		}
		
		if(loginRole.equalsIgnoreCase("ADMIN")) {
			return "admin-login-success";
		}
		return "success";
	}
	
	@SuppressWarnings("unused")
	public String validateUser_bk(LoginInfo loginInfo){
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Users users = null;
		try {
			Criteria criteria = session.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userId", loginInfo.getUserId()));
            criteria.add(Restrictions.eq("password", loginInfo.getPassword()));
            criteria.add(Restrictions.eq("deleteFlag", "N"));
            users = (Users) criteria.uniqueResult();
            
            if (users == null) {
            	ServletActionContext.getRequest().getSession().setAttribute("loggedInRole", "GUEST");
            	auditDao.createAudit(session, AuditUtils.getAudit(loginInfo, AuditUtils.LOGIN, AuditType.Login,"validateUser", criteria.toString(), "Login Fail"));
            	transaction.commit();
            	return "input";
			} else {
				//Audit Log
				ServletActionContext.getRequest().getSession().setAttribute("loggedInRole", users.getRole());
				ServletActionContext.getRequest().getSession().setAttribute("loggedNotifyGroup", users.getNotifyGroup());
	            auditDao.createAudit(session, AuditUtils.getAudit(loginInfo, AuditUtils.LOGIN, AuditType.Login,"validateUser", criteria.toString(), "Login Success"));
	            transaction.commit();
			}
            
		} catch (Exception e) {
			e.getMessage();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return "success";
	}
	
}
