package net.atos.user.dao;

import net.atos.user.bean.LoginInfo;

public interface LoginDAO {

	//public String validateUser(String userId, String password);
	public String validateUser(LoginInfo loginInfo);
}
