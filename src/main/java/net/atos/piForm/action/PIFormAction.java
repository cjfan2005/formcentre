package net.atos.piForm.action;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import org.apache.struts2.ServletActionContext;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import net.atos.interceptor.BaseAction;
import net.atos.piForm.bean.Piform;
import net.atos.piForm.bean.PiformId;
import net.atos.piForm.dao.PIFormDAO;
import net.atos.util.PageParameter;


public class PIFormAction extends BaseAction {
	public String country;
	public String countryAbbr;
	public String countryCode;
	public String nameOfTheNUSF; 
	public String address;
	public String city;
	public String postalCode;
	public String telephone1; 
	public String telephone2;
	public String mobile;
	public String telefax;
	public String email;
	public String name;
	public String function;
	public String signature;
	public String dateStr;
	public String status;
	public String isNewData;
	
	//gen PDF keys.(country&countryAbbr)
	String PDFConditions;
	String saveOrUptdateConditions;
	String formManageQueryKeys;
	
	// jqgrid
	private int total = 0; // 總頁數
	private int records = 0; // 資料總筆數
	private int pageSize = 0;
	private int currentPageNo = 0;
	private String sort;
	private String orderBy;
	private List<Map<String, Object>> rows;
	private Map result, countryMapJson;
	
	@Autowired
	private PIFormDAO pifDAO;
	
	public String piFormInit(){
		//取得目前登入使用者角色;國家/國家縮寫/國碼
		String loggedInCountry = ServletActionContext.getRequest().getSession().getAttribute("loggedCountry").toString();
		String loggedInCountryAbbr = ServletActionContext.getRequest().getSession().getAttribute("loggedCountryAbbr").toString();
		String loggedInCountryCode = ServletActionContext.getRequest().getSession().getAttribute("loggedCountryCode").toString();
		
		isNewData = "YES";
		country = loggedInCountry;
		countryAbbr = loggedInCountryAbbr;
		countryCode = loggedInCountryCode;
		
		Piform piFormBean = pifDAO.getPIFormByKey(country, countryAbbr);
		
		if(piFormBean != null) {
			isNewData = "NO";
			
			countryCode = piFormBean.getCountryCode();
			nameOfTheNUSF = piFormBean.getNameoftheNusf();
			address = piFormBean.getAddress();
			city = piFormBean.getCity();
			postalCode = piFormBean.getPostalCode();
			telephone1 = piFormBean.getTelephone1();
			telephone2 = piFormBean.getTelephone2();
			mobile = piFormBean.getMobile();
			telefax = piFormBean.getTelefax();
			email = piFormBean.getEmail();
			name = piFormBean.getName();
			function = piFormBean.getFunction();
			dateStr = piFormBean.getLastUpdateDate().toString();
			status = piFormBean.getStatus();
		}
		return "success";
	}
	
	
	//Save or update form data.
	public String piFormSubmit(){
		/*
		 Rule: 
		 1. The role of the 'ADMIN' could modify and approve all the forms.
		 */
		
		//get current user's role, ADMIN or USER.
		String logginedInRole = ServletActionContext.getRequest().getSession().getAttribute("loggedInRole").toString();
		
		String suc_c = "", suc_ca = "";
		if(saveOrUptdateConditions!=null && !saveOrUptdateConditions.isEmpty()) {
			String[] suc = saveOrUptdateConditions.split(";");
			suc_c = suc[0]; suc_ca = suc[1];
		}
		
		Piform piFormBean = null;
		PiformId piFormIdBean = null;
		
		piFormBean = new Piform();
		piFormIdBean = new PiformId();
		
		if(suc_c != null && !suc_c.isEmpty() && suc_ca != null && !suc_ca.isEmpty()) {
			piFormIdBean.setCountry(suc_c);
			piFormIdBean.setCountryAbbr(suc_ca);
		} else {
			piFormIdBean.setCountry(country);
			piFormIdBean.setCountryAbbr(countryAbbr);
		}
		
		piFormBean.setCountryCode(countryCode);
		piFormBean.setNameoftheNusf(nameOfTheNUSF);
		piFormBean.setAddress(address);
		piFormBean.setCity(city);
		piFormBean.setPostalCode(postalCode);
		piFormBean.setTelephone1(telephone1);
		piFormBean.setTelephone2(telephone2);
		piFormBean.setMobile(mobile);//it's means tel
		piFormBean.setTelefax(telefax);
		piFormBean.setEmail(email);
		piFormBean.setName(name);
		piFormBean.setFunction(function);
		
		piFormBean.setLastUpdateDate(new Date());
		
		if(logginedInRole.equalsIgnoreCase("ADMIN")) {
			isNewData = "NO";
			piFormBean.setStatus(status);}
		else if (isNewData.equals("YES"))
			piFormBean.setStatus("1");
		else if (status.equalsIgnoreCase("Reviewing"))
			piFormBean.setStatus("1");
		else
			piFormBean.setStatus("2");
		
		piFormBean.setId(piFormIdBean);
		
		String result = pifDAO.saveOrUpdatePIForm(piFormBean, isNewData);
		if(result.equals("succcess"))
			return "input";
		if(piFormBean.getStatus().equals("3")) {
			pifDAO.sendMailByGmail(piFormBean);
		}
		if(logginedInRole.equalsIgnoreCase("ADMIN")) {
			return "admin-modify-success";//admin user return.
		}
		return "success";//general user return.
	}

	
	//for ADMIN user.
	public String piFormMagInit() {
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String piFormManage() {
		
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);

		List<Map<String, Object>> objList = pifDAO.getPiForm(country, countryAbbr, status);
		int count = objList.size();

		objList = dataPagination(objList, page);

		Iterator<?> its = objList.iterator();
		List<Map<String, Object>> piFormList = new ArrayList<Map<String, Object>>();
		Map<String, Object> piFormMap = null;
		
		while (its.hasNext()) {
			piFormMap = new HashMap<String, Object>();

			Object[] obj = (Object[]) its.next();
            piFormMap.put("C&CA", obj[0] + " [" + obj[1] + "] ");
			piFormMap.put("country", obj[0]);
			piFormMap.put("countryAbbr", obj[1]);
			piFormMap.put("countryCode", obj[2]);
			piFormMap.put("nameOfTheNUSF", obj[3]);
			
			piFormMap.put("address", obj[4]);
			piFormMap.put("city", obj[5]);
			piFormMap.put("postalCode", obj[6]);
			piFormMap.put("telephone1", obj[7]);
			piFormMap.put("telephone2", obj[8]);
			piFormMap.put("mobile", obj[9]);
			piFormMap.put("telefax", obj[10]);
			piFormMap.put("email", obj[11]);
			piFormMap.put("name", obj[12]);
			piFormMap.put("function", obj[13]);
			
			piFormMap.put("lastUpdateDate", obj[14]);
			piFormMap.put("status", obj[15]);
			
			piFormList.add(piFormMap);
		}

		rows = piFormList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);

		return "success";
	}
	
	public String piFormMagDetail() {
		String fmqk_c ="", fmqk_ca="";
		
		if(formManageQueryKeys != null && !formManageQueryKeys.isEmpty()) {
			String[] fmqk = formManageQueryKeys.split("/");
			fmqk_c = fmqk[0];
			fmqk_ca = fmqk[1].replace(",", "");
		} else {
			return null;
		}
		
		Piform piFormBean = pifDAO.getPIFormByKey(fmqk_c, fmqk_ca);
		
		if(piFormBean != null) {
			isNewData = "NO";
			
			country = fmqk_c;
			countryAbbr = fmqk_ca;
			countryCode = piFormBean.getCountryCode();
			nameOfTheNUSF = piFormBean.getNameoftheNusf();
			address = piFormBean.getAddress();
			city = piFormBean.getCity();
			postalCode = piFormBean.getPostalCode();
			telephone1 = piFormBean.getTelephone1();
			telephone2 = piFormBean.getTelephone2();
			mobile = piFormBean.getMobile();
			telefax = piFormBean.getTelefax();
			email = piFormBean.getEmail();
			name = piFormBean.getName();
			function = piFormBean.getFunction();
			dateStr = piFormBean.getLastUpdateDate().toString();
			status = piFormBean.getStatus();
		}
		return "success";
	}

	public void genPiformPDF(){
		
		String[] pdfCondition = PDFConditions.split(";"); 
		String c = pdfCondition[0].trim();
		String ca = pdfCondition[1].trim();
		List<Piform> piList = pifDAO.getPIFormListByKey(c, ca);
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		InputStream stream = ServletActionContext.getServletContext().getResourceAsStream("/PDFTemplate/PIForm.pdf");
		try {
			PdfReader reader = new PdfReader(stream);
			PdfStamper stamp = new PdfStamper(reader,buffer);
			AcroFields pdfForm = stamp.getAcroFields();
			
			//TODO piList.get(0).getId().getCountry()
			pdfForm.setField("Country", c);
			pdfForm.setField("CountryAbbr", ca);
			pdfForm.setField("NameoftheNUSF", piList.get(0).getNameoftheNusf().trim());
			pdfForm.setField("Address", piList.get(0).getAddress());
			pdfForm.setField("City", piList.get(0).getCity());
			pdfForm.setField("PostalCode", piList.get(0).getPostalCode());
			pdfForm.setField("Telephone1", piList.get(0).getTelephone1());
			pdfForm.setField("Telephone2", piList.get(0).getTelephone2());
			pdfForm.setField("Telefax", piList.get(0).getTelefax());
			pdfForm.setField("Email", piList.get(0).getEmail());
			
			stamp.setFormFlattening(true);
			stamp.close();
		
			byte[] pdfByte = buffer.toByteArray();
			
			String pdfFileName = ca +"_PIForm.pdf";
			ServletActionContext.getResponse().setContentType("application/pdf");
			ServletActionContext.getResponse().setHeader("Expires", "0");
			ServletActionContext.getResponse().setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			ServletActionContext.getResponse().setHeader("Content-Disposition", "attachment; filename=" + pdfFileName);
			ServletActionContext.getResponse().setContentLength((int) pdfByte.length);
			ServletOutputStream os = ServletActionContext.getResponse().getOutputStream();
			os.write(pdfByte);
			os.flush();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getCountryMapJSON() {
		
		
		List<Map<String, Object>> objList = pifDAO.getCountryMapJSON();
		
		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> cmList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> cmItem;
		countryMapJson = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			cmItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			cmItem.put("country", obj[0].toString());
			cmItem.put("countryAbbr", obj[1]);
			cmItem.put("countryCode", obj[2]);
			cmList.add(cmItem);
		}
		
		countryMapJson.put("countryMapJsonStr", cmList);
		return "success";
	}
	
	
	//getter and setter 
	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryAbbr() {
		return countryAbbr;
	}

	public void setCountryAbbr(String countryAbbr) {
		this.countryAbbr = countryAbbr;
	}

	public String getCountryCode() {
		return countryCode;
	}


	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	public String getNameOfTheNUSF() {
		return nameOfTheNUSF;
	}


	public void setNameOfTheNUSF(String nameOfTheNUSF) {
		this.nameOfTheNUSF = nameOfTheNUSF;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getPostalCode() {
		return postalCode;
	}


	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getTelephone1() {
		return telephone1;
	}

	public void setTelephone1(String telephone1) {
		this.telephone1 = telephone1;
	}

	public String getTelephone2() {
		return telephone2;
	}

	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}

	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getTelefax() {
		return telefax;
	}


	public void setTelefax(String telefax) {
		this.telefax = telefax;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getFunction() {
		return function;
	}


	public void setFunction(String function) {
		this.function = function;
	}


	public String getSignature() {
		return signature;
	}


	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getDateStr() {
		return dateStr;
	}


	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getIsNewData() {
		return isNewData;
	}

	public void setIsNewData(String isNewData) {
		this.isNewData = isNewData;
	}


	public String getPDFConditions() {
		return PDFConditions;
	}


	public void setPDFConditions(String pDFConditions) {
		PDFConditions = pDFConditions;
	}


	public String getFormManageQueryKeys() {
		return formManageQueryKeys;
	}


	public void setFormManageQueryKeys(String formManageQueryKeys) {
		this.formManageQueryKeys = formManageQueryKeys;
	}


	public int getTotal() {
		return total;
	}


	public void setTotal(int total) {
		this.total = total;
	}


	public int getRecords() {
		return records;
	}


	public void setRecords(int records) {
		this.records = records;
	}


	public int getPageSize() {
		return pageSize;
	}


	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}


	public int getCurrentPageNo() {
		return currentPageNo;
	}


	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}


	public String getSort() {
		return sort;
	}


	public void setSort(String sort) {
		this.sort = sort;
	}


	public String getOrderBy() {
		return orderBy;
	}


	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}


	public List<Map<String, Object>> getRows() {
		return rows;
	}


	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}


	public Map getResult() {
		return result;
	}


	public void setResult(Map result) {
		this.result = result;
	}


	public String getSaveOrUptdateConditions() {
		return saveOrUptdateConditions;
	}


	public void setSaveOrUptdateConditions(String saveOrUptdateConditions) {
		this.saveOrUptdateConditions = saveOrUptdateConditions;
	}


	public Map getCountryMapJson() {
		return countryMapJson;
	}


	public void setCountryMapJson(Map countryMapJson) {
		this.countryMapJson = countryMapJson;
	}


	
}
