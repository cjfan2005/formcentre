package net.atos.piForm.dao;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import net.atos.piForm.bean.Piform;

public interface PIFormDAO {
    public String saveOrUpdatePIForm(Piform piFormBean, String isNewData);
	public List<Piform> getPIFormListByKey(String country, String countryAbbr);
	public Piform getPIFormByKey(String country, String countryAbbr);
	
	public List<Map<String, Object>> getPiForm(String country, String countryAbbr, String status);
	
	public List<Map<String, Object>> getCountryMapJSON();
	public String sendMailByGmail(Piform piFormBean);
}
