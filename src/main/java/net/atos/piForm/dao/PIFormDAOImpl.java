package net.atos.piForm.dao;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

import net.atos.piForm.bean.Piform;
import net.atos.piForm.bean.PiformId;
import net.atos.util.StringUtils;

public class PIFormDAOImpl implements PIFormDAO {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	//save or update Piform with Bean
	public String saveOrUpdatePIForm(Piform piFormBean, String isNewData) {
		
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			
			if(isNewData.equals("NO")) {
				//update
				session.update(piFormBean);
			} else {
				//save
				session.save(piFormBean);
			}
			t.commit();
			
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(session.isOpen())
				session.close();
		}
		
		return "success";
	}
	
	//get PI Form Data by Country
	public List<Piform> getPIFormListByKey(String country, String countryAbbr) {
		Session session = null;
		session = sessionFactory.openSession();
		
		Criteria c = session.createCriteria(Piform.class);
		c.add(Restrictions.eq("id.country", country));
		c.add(Restrictions.eq("id.countryAbbr", countryAbbr));
		
		List<Piform> piList = c.list();
		session.close();
		return piList;
	}
	
	//get PI Form Data by Country
	public Piform getPIFormByKey(String country, String countryAbbr) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		Piform piFormBean = null;
		PiformId piFormIdBean = null;
		String sql = "";
		try {
			sql += " SELECT ";
			sql += " cm.Country, cm.CountryAbbr, pi.CountryCode, pi.NameoftheNUSF, pi.Address, pi.City, pi.PostalCode,";
			sql += " pi.Telephone1, pi.Telephone2, pi.Mobile, pi.Telefax,  ";
			sql += " pi.Email, pi.Name, pi.Function, pi.LastUpdateDate, sm.StatusName ";
			sql += " FROM ";
			sql += " piform as pi ";
			sql += " LEFT JOIN StatusMap sm  ON sm.StatusCode = pi.Status ";
			sql += " LEFT JOIN CountryMap cm  ON cm.Country = pi.Country AND  cm.CountryAbbr = pi.CountryAbbr ";
			sql += " WHERE ";
			sql += " pi.Country = '"+ country +"' AND";
			sql += " pi.CountryAbbr = '"+ countryAbbr +"' ";
						
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
			
			if(list.size() > 0) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				//arrange
				Iterator<?> its = list.iterator();
				while (its.hasNext()) {
					Object[] obj = (Object[]) its.next();
					piFormBean = new Piform();
					piFormIdBean = new PiformId();
					
					piFormIdBean.setCountry(obj[0].toString());
					piFormIdBean.setCountryAbbr(obj[1].toString());
					piFormBean.setCountryCode(obj[2].toString());
					piFormBean.setNameoftheNusf(obj[3].toString());
					piFormBean.setAddress(obj[4].toString());
					piFormBean.setCity(obj[5].toString());
					piFormBean.setPostalCode(obj[6].toString());
					piFormBean.setTelephone1(obj[7].toString());
					piFormBean.setTelephone2(obj[8].toString());
					piFormBean.setMobile(obj[9].toString());
					piFormBean.setTelefax(obj[10].toString());
					piFormBean.setEmail(obj[11].toString());
					piFormBean.setName(obj[12].toString());
					if(obj[13]==null)
						piFormBean.setFunction("NA");
					else
						piFormBean.setFunction(obj[13].toString());
					piFormBean.setLastUpdateDate(sdf.parse(obj[14].toString()));
					piFormBean.setStatus(obj[15].toString());
					piFormBean.setId(piFormIdBean);
				}
			} else {
				return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return piFormBean;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getPiForm(String country, String countryAbbr, String status) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		
		String sql = "";
		try {
			sql += " SELECT ";
			sql += " cm.Country, cm.CountryAbbr, pi.CountryCode, pi.NameoftheNUSF, pi.Address, pi.City, pi.PostalCode,";
			sql += " pi.Telephone1, pi.Telephone2, pi.Mobile, pi.Telefax, ";
			sql += " pi.Email, pi.Name, pi.Function, ";
			sql += " DATE_FORMAT(pi.LastUpdateDate,'%Y-%m-%d %H:%I:%S') as luDate, ";
			sql += " sm.StatusName";
			sql += " FROM ";
			sql += " piform as pi ";
			sql += " LEFT JOIN StatusMap sm ON sm.StatusCode = pi.Status ";
			sql += " LEFT JOIN CountryMap cm ON cm.Country = pi.Country AND cm.CountryAbbr = pi.CountryAbbr ";
			sql += " WHERE 1=1 ";
			
			if(StringUtils.isNotBlank(country))
				sql += " AND pi.Country = '" + country + "' ";
			
			if(StringUtils.isNotBlank(countryAbbr))
				sql += " AND pi.CountryAbbr = '" + countryAbbr + "' ";
			
			if(StringUtils.isNotBlank(status))
				sql += " AND pi.status = '" + status + "' ";
			
			sql += " Order by pi.Country ASC; ";
						
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getCountryMapJSON() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += " SELECT Country, CountryAbbr, CountryCode FROM CountryMap Order by Country ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public String sendMailByGmail(Piform piFormBean) {
		String host = "smtp.gmail.com";
		int port = 587;
		final String username = "rocky3271@gmail.com";
		final String password = "applecar555";

		String sendMail = piFormBean.getEmail();

		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", port);
		javax.mail.Session session = javax.mail.Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("fromn@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(sendMail));
			message.setSubject("測試寄信包含圖片及文字.");

			message.setText("Dear Levin, \n\n 測試 測試 測試 測試 測試 測試 email !");
			//HTML and PIC add(新增html及圖片後，上方輸入的text將被覆蓋)
//			MimeMultipart multipart = new MimeMultipart("related");
//			BodyPart messageBodyPart = new MimeBodyPart();
//			String htmlText = "<H1>Hello</H1><img src=\"cid:image\">";
//			messageBodyPart.setContent(htmlText, "text/html");
//			multipart.addBodyPart(messageBodyPart);
//			// second part (the image)設定上傳圖片路徑，名稱需對應
//			messageBodyPart = new MimeBodyPart();
//			DataSource fds = new FileDataSource("C:\\809.jpg");
//			messageBodyPart.setDataHandler(new DataHandler(fds));
//			messageBodyPart.setHeader("Content-ID", "<image>");
//			multipart.addBodyPart(messageBodyPart);
//
//			message.setContent(multipart);
			
			Transport transport = session.getTransport("smtp");
			transport.connect(host, port, username, password);

			Transport.send(message);
			System.out.println("寄送email結束.");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return null;
	}
}
