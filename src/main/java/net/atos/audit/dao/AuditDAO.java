package net.atos.audit.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import net.atos.audit.bean.Audit;

public interface AuditDAO {
	
	/**
	 * 寫入AuditLog
	 * @param session
	 * @param audit
	 */
	public void createAudit(Session session, Audit audit);
	
	public List<Map<String, Object>> getAllAudit(String order, String sort);
	
	public List<Map<String, Object>> getAuditCondition(String userId, String auditType, String startDate, String endDate, String order, String sort);
}
