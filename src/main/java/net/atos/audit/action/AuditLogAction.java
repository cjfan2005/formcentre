package net.atos.audit.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import net.atos.audit.dao.AuditDAO;
import net.atos.interceptor.BaseAction;
import net.atos.util.StringUtils;
import net.atos.util.PageParameter;

public class AuditLogAction extends BaseAction {

	@Autowired
	private AuditDAO auditDAO;
	// 接收前端參數
	private List<Map<String, Object>> rows;
	private Map result;

	private String date;
	private String dateRange;
	private String userId;
	private String auditType;

	private int total = 0; // 總頁數
	private int records = 0; // 資料總筆數
	private int pageSize = 0;
	private int currentPageNo = 0;
	private String sort;
	private String orderBy;

	@SuppressWarnings("unchecked")
	public String SearchAuditLogJSON() throws Exception {
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);

		String start = "";
		String end = "";

		Calendar c = Calendar.getInstance();

		if (StringUtils.isBlank(date)) {
			end = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
			c.setTime(new Date());
		} else {
			end = date;
			c.setTime(new SimpleDateFormat("yyyy/MM/dd").parse(date));
		}

		if (StringUtils.isBlank(dateRange))
			dateRange = "1";

		switch (Integer.parseInt(dateRange)) {
		case 1:
			c.add(Calendar.MONTH, -1);
			break;
		case 2:
			c.add(Calendar.MONTH, -2);
			break;
		case 3:
			c.add(Calendar.MONTH, -3);
			break;
		}

		start = new SimpleDateFormat("yyyy/MM/dd").format(c.getTime());

		start = start.replace("/", "-") + " 00:00:00";
		end = end.replace("/", "-") + " 23:59:59";

		List<Map<String, Object>> objList = auditDAO.getAuditCondition(userId, auditType, start, end, orderBy, sort);
		int count = objList.size();

		objList = dataPagination(objList, page);

		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> auditLogList = new ArrayList<Map<String, Object>>();
		Map<String, Object> auditLog;

		while (its.hasNext()) {
			auditLog = new HashMap<String, Object>();

			Object[] obj = (Object[]) its.next();

			auditLog.put("UserId", obj[0]);
			auditLog.put("AuditTime", obj[1]);
			auditLog.put("ProgramName", obj[2]);
			auditLog.put("AuditType", obj[3]);
			auditLog.put("SqlLog", obj[4]);

			auditLogList.add(auditLog);
		}

		rows = auditLogList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String InitAuditLogJSON() {

		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		List<Map<String, Object>> objList = auditDAO.getAllAudit(orderBy, sort);
		int count = objList.size();

		objList = dataPagination(objList, page);

		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> auditLogList = new ArrayList<Map<String, Object>>();
		Map<String, Object> auditLog;

		while (its.hasNext()) {

			auditLog = new HashMap<String, Object>();

			Object[] obj = (Object[]) its.next();

			auditLog.put("UserId", obj[2]);
			auditLog.put("AuditTime", obj[1]);
			auditLog.put("ProgramName", obj[3]);
			auditLog.put("AuditType", obj[4]);
			auditLog.put("SqlLog", obj[7]);

			auditLogList.add(auditLog);
		}

		rows = auditLogList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);

		return SUCCESS;
	}

	public String InitAuditLog() {

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String searchAuditLog() throws Exception {

		return SUCCESS;
	}

	public List<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDateRange() {
		return dateRange;
	}

	public void setDateRange(String dateRange) {
		this.dateRange = dateRange;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Map getResult() {
		return result;
	}

	public void setResult(Map result) {
		this.result = result;
	}

	public String getAuditType() {
		return auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

}
