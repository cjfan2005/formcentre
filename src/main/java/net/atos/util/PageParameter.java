package net.atos.util;



/**
 * Jqgrid 參數
 */
public class PageParameter implements java.io.Serializable {

	private static final long serialVersionUID = 4733575687473446908L;
	
	private int currentPageNo;  //當前頁
	private int pageSize;          //每頁顯示資料筆數
	private String orderBy;  	   //排序方式
	private String sort;		   //排序欄位
	private int records;           //資料總筆數
	private int total;             //總頁數

	public PageParameter() {
	}
	
	public PageParameter(int currentPageNo, int pageSize, String orderBy, String sort, int records, int total) {
		super();
		this.currentPageNo = currentPageNo;
		this.pageSize = pageSize;
		this.orderBy = orderBy;
		this.sort = sort;
		this.records = records;
		this.total = total;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "PageParameter [currentPageNo=" + currentPageNo + ", pageSize=" + pageSize + ", orderBy=" + orderBy + ", sort=" + sort + ", records=" + records + ", total=" + total + "]";
	}
}
