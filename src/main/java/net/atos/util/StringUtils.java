package net.atos.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;

/**
 * 字串處理工具集
 * <p>
 * 字串工具集主要功能繼承自{@link org.apache.commons.lang.StringUtils}， 並增加下列功能：
 * <ul>
 * <li>金額型態字串處理功能</li>
 * <li>字串切割功能</li>
 * </ul>
 * </p>
 * 
 * @author Jeff Liu
 * @version 1.0, 2004/10/18
 * @see {@link org.apache.commons.lang.StringUtils}
 * @since
 */
public class StringUtils extends org.apache.commons.lang.StringUtils {
	public static final Pattern TW_PERSONAL_ID_PATTERN = Pattern.compile("[a-zA-Z]\\d{9}$");
	public static final Pattern TW_COMPANY_ID_PATTERN = Pattern.compile("^[0-9]{8}$");
	

	public static void main(String[] args) {

		String str = "01234.12345";

		String decimal = ".";
		
		String[] strArr = str.split("\\" + decimal);
		
		if(strArr.length != 0){
			System.out.println("true");
		}else{
			System.out.println("false");
		}
		
		System.out.println("moneyStr = " + getMoneyStr(str));
		
		System.out.println(str.indexOf(decimal));
		
		str = str.substring(str.indexOf(decimal));
		
		System.out.println(str);
		
//		String sysoStr = getCoinStr(str);
//
//		System.out.println(sysoStr);

	}

	/**
	 * 根據字串長度切割字串，一個中文字長度是1
	 * 
	 * <pre>
	 * StringUtils.getTokens(null, *)         	= []
	 * StringUtils.getTokens(&quot;&quot;, *)           	= []
	 * StringUtils.getTokens(&quot;abc def&quot;, -1) 	= []
	 * StringUtils.getTokens(&quot;abc def&quot;, 0)  	= []
	 * StringUtils.getTokens(&quot;abc  def&quot;, 2) 	= [&quot;ab&quot;, &quot;c &quot;, &quot;de&quot;, &quot;f&quot;]
	 * </pre>
	 * 
	 * @param sData 原始字串
	 * @param iLength 切割的長度
	 * @return 字串陣列，永不為null
	 */
	public static String[] getTokens(String sData, int iLength) {

		if (null == sData || iLength < 1) {
			return ArrayUtils.EMPTY_STRING_ARRAY;
		}

		List tokens = new ArrayList();

		int iLeft = 0;

		int iDataLen = sData.length();

		while (iLeft < iDataLen) {

			int iRight = (iLeft + iLength) > iDataLen ? iDataLen : iLeft + iLength;

			String sToken = sData.substring(iLeft, iRight);

			iLeft += iLength;

			tokens.add(sToken);
		}

		return (String[]) tokens.toArray(new String[tokens.size()]);
	}

	/**
	 * 根據字串實際的長度切割字串
	 * 
	 * <pre>
	 * 	StringUtils.getTokens(null, *)         	= []
	 * 	StringUtils.getTokens(&quot;&quot;, *)           	= []
	 * 	StringUtils.getTokens(&quot;abc def&quot;, -1) 	= []
	 * 	StringUtils.getTokens(&quot;abc def&quot;, 0)  	= []
	 * StringUtils.getTokens(&quot;abc  def&quot;, 2) 	= [&quot;ab&quot;, &quot;c &quot;, &quot;de&quot;, &quot;f&quot;]
	 * </pre>
	 * 
	 * @param sData 原始字串
	 * @param iLength 切割的長度(byte)
	 * @return 切割後的字串陣列，永不為NULL
	 */
	public static String[] getTokensByBytes(String sData, int iLength) {

		if (null == sData || iLength < 1) {
			return ArrayUtils.EMPTY_STRING_ARRAY;
		}

		List tokens = new ArrayList();

		byte[] datas = sData.getBytes();

		int iLeft = 0;

		int iTotalLength = datas.length;

		while (iLeft < iTotalLength) {

			int iTokenLength = (iLeft + iLength) > iTotalLength ? iTotalLength - iLeft : iLength;

			byte[] token = new byte[iTokenLength];

			System.arraycopy(datas, iLeft, token, 0, iTokenLength);

			iLeft += iLength;

			tokens.add(new String(token));
		}

		return (String[]) tokens.toArray(new String[tokens.size()]);
	}

	/**
	 * 根據分隔子切割字串
	 * 
	 * <pre>
	 * StringUtils.getTokens(null, *)         	= []
	 * StringUtils.getTokens(&quot;&quot;, *)           	= []
	 * StringUtils.getTokens(&quot;abc def&quot;, null) 	= [&quot;abc&quot;, &quot;def&quot;]
	 * StringUtils.getTokens(&quot;abc def&quot;, &quot; &quot;)  	= [&quot;abc&quot;, &quot;def&quot;]
	 * StringUtils.getTokens(&quot;abc  def&quot;, &quot; &quot;) 	= [&quot;abc&quot;, &quot;def&quot;]
	 * StringUtils.getTokens(&quot;ab:cd:ef&quot;, &quot;:&quot;) 	= [&quot;ab&quot;, &quot;cd&quot;, &quot;ef&quot;]
	 * StringUtils.getTokens(&quot;ab:cd:ef:&quot;, &quot;:&quot;) = [&quot;ab&quot;, &quot;cd&quot;, &quot;ef&quot;, &quot;&quot;]
	 * </pre>
	 * 
	 * @param sData 原始字串
	 * @param iLength 切割的長度(byte)
	 * @return 切割後的字串陣列，永不為NULL
	 */
	public static String[] getTokens(String sData, String sDelim) {

		if (null == sData) {
			return ArrayUtils.EMPTY_STRING_ARRAY;
		}

		List tokens = new ArrayList();

		int iDataLen = sData.length();
		int iDelimLen = sDelim.length();

		int iLeft = 0;
		int iRight = sData.indexOf(sDelim);

		while (iRight >= 0) {

			String sToken = sData.substring(iLeft, iRight).trim();
			tokens.add(sToken);
			iLeft = iRight + iDelimLen;
			iRight = sData.indexOf(sDelim, iLeft);
		}

		if (iLeft < iDataLen) {
			String sToken = sData.substring(iLeft, iDataLen);
			tokens.add(sToken);
		}

		// 取最後一個token，如果為delim則加入一個空白("")token
		if (iDataLen >= iDelimLen) {
			String sLastToken = sData.substring(iDataLen - iDelimLen, iDataLen);

			if (sLastToken.equals(sDelim)) {
				tokens.add("");
			}
		}
		return (String[]) tokens.toArray(new String[tokens.size()]);

	}

	/**
	 * 取代字串中的分隔子
	 * 
	 * <pre>
	 * StringUtils.replaceDelim(null, null, null)			= null
	 * StringUtils.replaceDelim(&quot;&quot;, null, null)           	= &quot;&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*, &quot;*&quot;, []) 			= &quot;a*b*c*&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*&quot;, &quot;*&quot;, null)  		= &quot;a*b*c*&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*&quot;, &quot;*&quot;, [1])    	= &quot;a*b*c*&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*&quot;, &quot;*&quot;, [1, 2, 3])  = &quot;a1b2c3&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*&quot;, &quot;&amp;&quot;, [1, 2, 3])  = &quot;a*b*c*&quot;
	 * </pre>
	 * 
	 * @param sSource string to seach and replace in, may be null
	 * @param sDelim the string to search for, may be null
	 * @param with the strings to replace with
	 * @return
	 */
	public static String replaceDelim(String sSource, String sDelim, String with[]) {

		if (sSource == null || sDelim == null || with == null || sDelim.length() == 0 || with.length == 0) {
			return sSource;
		}

		String[] tokens = StringUtils.getTokens(sSource, sDelim);

		// token number - 1 == with number
		if ((tokens.length - 1) != with.length) {
			return sSource;
		}

		StringBuffer sb = new StringBuffer("");

		for (int i = 0; i < with.length; i++) {

			sb.append(tokens[i]);

			sb.append(with[i]);
		}

		// last token
		sb.append(tokens[tokens.length - 1]);

		return sb.toString();

	}

	/**
	 * 取代字串中的分隔子
	 * 
	 * <pre>
	 * StringUtils.replaceDelim(null, null, null)			= null
	 * StringUtils.replaceDelim(&quot;&quot;, null, null)           	= &quot;&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*, &quot;*&quot;, [])			= &quot;a*b*c*&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*&quot;, &quot;*&quot;, null)  		= &quot;a*b*c*&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*&quot;, &quot;*&quot;, [1])   		= &quot;a*b*c*&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*&quot;, &quot;*&quot;, [1, 2, 3])  	= &quot;a1b2c3&quot;
	 * StringUtils.replaceDelim(&quot;a*b*c*&quot;, &quot;&amp;&quot;, [1, 2, 3])	= &quot;a*b*c*&quot;
	 * </pre>
	 * 
	 * @param sSource string to seach and replace in, may be null
	 * @param sDelim the string to search for, may be null
	 * @param with the strings to replace with
	 * @return
	 */
	public static String replaceDelim(String sSource, String sDelim, List with) {

		String[] array = (String[]) with.toArray(new String[with.size()]);

		return replaceDelim(sSource, sDelim, array);

	}

	/**
	 * 將字串轉換為數字，如果轉換失敗則回傳預設值
	 * 
	 * <pre>
	 * ConvertUtils.str2Int(null) = 0
	 * ConvertUtils.str2Int(&quot;abc&quot;) = 0
	 * ConvertUtils.str2Int(&quot;1&quot;) = 1
	 * </pre>
	 * 
	 * @param sValue 待轉換之字串
	 * @param iDefaultValue 轉換失敗時之預設值
	 * @return 轉換後之數字，失敗則傳回iDefaultValue
	 * @deprecated 請改使用{@link com.ibm.tw.util.ConvertUtils.str2Int}
	 */
	public static int str2Int(String sValue, int iDefaultValue) {
		int iValue = iDefaultValue;
		try {
			iValue = Integer.parseInt(sValue);
		} catch (Exception e) {
			iValue = iDefaultValue;
		}

		return iValue;
	}



	/**
	 * 將字串轉換為金額的表示方式
	 * 
	 * <pre>
	 * StringUtils.getMoneyStr(null) = &quot;&quot;
	 * StringUtils.getMoneyStr(&quot; &quot;) = &quot;&quot;
	 * StringUtils.getMoneyStr(&quot;1234567.89&quot;) = &quot;1,234,567.89&quot;
	 * StringUtils.getMoneyStr(&quot;1234567.00&quot;) = &quot;1,234,567&quot;
	 * StringUtils.getMoneyStr(&quot;1234567.0100&quot;) = &quot;1,234,567.01&quot;
	 * </pre>
	 * 
	 * @param sMoney
	 * @return
	 */
	public static String getMoneyStr(String sMoney) {

		if (isBlank(sMoney)) {
			return "";
		}

		sMoney = sMoney.trim();

		StringBuffer sb = new StringBuffer();
		int iLen = sMoney.length();

		if (sMoney.startsWith("+") || sMoney.startsWith("-")) {

			String sSign = substring(sMoney, 0, 1);
			// '-' 放回去, '+' 濾除
			sb.append(sSign.equals("-") ? sSign : "");
			sMoney = substring(sMoney, 1);
		}

		// 整數
		String sInt = "";

		// 小數
		String sDecimal = "";

		// 小數點
		String sDot = "";

		int index = sMoney.indexOf(".");

		if (index >= 0) {
			sDot = ".";
			sInt = sMoney.substring(0, index);
			if ((index + 1) < iLen) {
				sDecimal = sMoney.substring(index + 1, sMoney.length());
			}
		} else {
			sInt = sMoney;
		}

		// 整數
		sInt = getIntMoneyStr(sInt);

		// 小數
		sDecimal = trimRightZero(sDecimal);

		// 有整數部分
		if (sInt.length() > 0) {
			sb.append(sInt);

			if (sDecimal.length() > 0) {
				sb.append(sDot).append(sDecimal);
			}
		}
		// 沒有整數部分
		else {
			if (sDecimal.length() > 0) {
				sb.append("0.").append(sDecimal);
			} else {
				sb.append("0");
			}
		}

		return sb.toString();
	}

	/**
	 * 將字串轉換為金額的表示方式
	 * 
	 * <pre>
	 * StringUtils.getMoneyStr(null, 1) = &quot;&quot;
	 * StringUtils.getMoneyStr(&quot; &quot;, 2) = &quot;&quot;
	 * StringUtils.getMoneyStr(&quot;1234567.89&quot;, 2) = &quot;1,234,567.89&quot; 
	 * StringUtils.getMoneyStr(&quot;1234567.00&quot;, 1) = &quot;1,234,567.0&quot;;
	 * StringUtils.getMoneyStr(&quot;1234567.0100&quot;, 2) = &quot;1,234,567.01&quot;;
	 * </pre>
	 * 
	 * @param sMoney 待轉換之字串
	 * @param iScale 小數有效位數
	 * @return 金額表示型態的字串，永不為null
	 */
	public static String getMoneyStr(String sMoney, int iScale) {

		if (isBlank(sMoney)) {
			return "";
		}

		sMoney = sMoney.trim();

		StringBuffer sb = new StringBuffer();
		int iLen = sMoney.length();

		if (sMoney.startsWith("+") || sMoney.startsWith("-")) {

			String sSign = substring(sMoney, 0, 1);
			// '-' 放回去, '+' 濾除
			sb.append(sSign.equals("-") ? sSign : "");
			sMoney = substring(sMoney, 1);
		}

		// 整數
		String sInt = "";

		// 小數
		String sDecimal = "";

		// 小數點
		String sDot = "";

		int index = sMoney.indexOf(".");

		if (index >= 0) {
			sDot = ".";
			sInt = sMoney.substring(0, index);
			if ((index + 1) < iLen) {
				sDecimal = sMoney.substring(index + 1, sMoney.length());
			}
		} else {
			sInt = sMoney;
		}

		// 整數
		sInt = getIntMoneyStr(sInt);

		// 小數
		sDecimal = substring(sDecimal, 0, iScale);

		// 有整數部分
		if (sInt.length() > 0) {
			sb.append(sInt);

			if (sDecimal.length() > 0) {
				sb.append(sDot).append(rightPad(sDecimal, iScale, "0"));
			} else {
				if (iScale > 0) {
					sb.append(".").append(rightPad(sDecimal, iScale, "0"));
				}
			}
		}
		// 沒有整數部分
		else {
			if (sDecimal.length() > 0) {
				sb.append("0.").append(rightPad(sDecimal, iScale, "0"));
			} else {
				if (iScale > 0) {
					sb.append("0.").append(rightPad(sDecimal, iScale, "0"));
				} else {
					sb.append("0");
				}
			}
		}

		return sb.toString();
	}

	/**
	 * 將字串轉換為金額表示型態的字串
	 * 
	 * <pre>
	 * StringUtils.getIntMoneyStr(&quot;001234567&quot;) = &quot;1,234,567&quot;
	 * </pre>
	 * 
	 * @param sInt
	 * @return
	 * @deprecated 請改用{@link com.ibm.tw.utils.StringUtils.getMoneyStr}
	 */
	public static String getIntMoneyStr(String sInt) {

		if (isBlank(sInt)) {
			return "";
		}

		sInt = sInt.trim();

		StringBuffer sb = new StringBuffer();

		sInt = trimLeftZero(sInt);
		int iLen = sInt.length();

		for (int i = 0; i < iLen; i++) {

			char ch = sInt.charAt(i);

			sb.append(ch);
			// 剩餘長度
			int iRemainLen = iLen - i - 1;

			if ((iRemainLen > 0) && (iRemainLen % 3 == 0)) {
				sb.append(",");
			}

		}
		return sb.toString();

	}

	/**
	 * 移除字串左邊的<code>0</code>字元
	 * 
	 * <pre>
	 * StringUtils.trimLeftZero(null) = &quot;&quot;
	 * StringUtils.trimLeftZero(&quot;0012345600&quot;) = &quot;12345600&quot;
	 * </pre>
	 * 
	 * @param sSource 待處理之字串
	 * @return 移除左側<code>0</code>字元後的字串，永不為null
	 */
	public static String trimLeftZero(String sSource) {

		if (sSource == null) {
			return "";
		}

		int iLen = sSource.length();
		int index = -1;
		for (int i = 0; (i < iLen && index < 0); i++) {
			char ch = sSource.charAt(i);

			if (ch != '0') {
				index = i;
			}
		}
		String s = "";
		// 發現非0之數字
		if (index >= 0) {
			s = sSource.substring(index, iLen);
		}

		return s;

	}

	/**
	 * StringUtils.trimRightZero("01234500") = "012345"
	 * 
	 * @param sSource
	 * @return
	 */
	public static String trimRightZero(String sSource) {

		if (sSource == null) {
			return "";
		}

		int iLen = sSource.length();
		int index = -1;
		// 由後至前
		for (int i = (iLen - 1); (i >= 0 && index < 0); i--) {
			char ch = sSource.charAt(i);
			if (ch != '0') {
				index = i;
			}
		}

		String s = "";
		if (index >= 0) {
			s = sSource.substring(0, index + 1);
		}

		return s;
	}

	/**
	 * 將字串轉換為百分比顯示型態
	 * 
	 * <pre>
	 * StringUtils.getRateStr(null) = &quot;&quot;
	 * StringUtils.getRateStr(&quot;000122.00100&quot;) = &quot;122.001&quot;
	 * StringUtils.getRateStr(&quot;00.12&quot;) = &quot;0.12&quot;
	 * StringUtils.getRateStr(&quot;00.00&quot;) = &quot;0.0&quot;
	 * </pre>
	 * 
	 * @param sRate
	 * @return 轉換後之百分比顯示型態字串，永不為null
	 */
	public static String getRateStr(String sRate) {
		if (StringUtils.isBlank(sRate)) {
			return "";
		}

		StringBuffer sb = new StringBuffer();
		int iLen = sRate.length();

		// 整數
		String sInt = "";

		// 小數
		String sDecimal = "";

		// 小數點
		String sDot = "";

		int index = sRate.indexOf(".");

		if (index >= 0) {
			sDot = ".";
			sInt = sRate.substring(0, index);
			if ((index + 1) < iLen) {
				sDecimal = sRate.substring(index + 1, sRate.length());
			}
		} else {
			sInt = sRate;
		}

		// 整數
		sInt = trimLeftZero(sInt);

		// 小數
		sDecimal = trimRightZero(sDecimal);

		// 有整數部分
		if (sInt.length() > 0) {
			sb.append(sInt);

			if (sDecimal.length() > 0) {
				sb.append(sDot).append(sDecimal);
			}
		}
		// 沒有整數部分
		else {
			if (sDecimal.length() > 0) {
				sb.append("0.").append(sDecimal);
			} else {
				sb.append("0.0");
			}
		}

		// sb.append("%");

		return sb.toString();
	}

	/**
	 * 取得字串中的整數部分
	 * 
	 * <pre>
	 * StringUtils.getRateStr(null) = &quot;&quot;
	 * StringUtils.getRateStr(&quot;000122.00100&quot;) = &quot;122&quot;
	 * </pre>
	 * 
	 * @param sRate
	 * @return
	 */
	public static String getIntPart(String sRate) {

		if (StringUtils.isBlank(sRate)) {
			return "";
		}

		// 整數
		String sInt = "";

		int index = sRate.indexOf(".");

		if (index >= 0) {
			sInt = sRate.substring(0, index);
		} else {
			sInt = sRate;
		}

		// 整數
		sInt = trimLeftZero(sInt);

		return sInt;
	}

	/**
	 * 以 byte length 計算 padding
	 * 
	 * @param sData
	 * @param iLength
	 * @param sPadStr
	 * @return
	 */
	public static String leftPadByByteLenth(String sData, int iLength, String sPadStr) {

		if (sData == null) {
			return null;
		}

		if (sPadStr == null || sPadStr.length() == 0) {
			sPadStr = " ";
		}

		String sResult = sData;
		while (sResult.getBytes().length < iLength) {
			sResult = sPadStr + sResult;
		}

		return sResult;
	}

	/**
	 * 以 byte length 計算 padding
	 * 
	 * @param sData
	 * @param iLength
	 * @param sPadStr
	 * @return
	 */
	public static String rightPadByByteLenth(String sData, int iLength, String sPadStr) {

		if (sData == null) {
			return null;
		}

		if (sPadStr == null || sPadStr.length() == 0) {
			sPadStr = " ";
		}
		String sResult = sData;

		while (sResult.getBytes().length < iLength) {
			sResult += sPadStr;
		}

		return sResult;
	}

	/**
	 * 以 byte length 計算 padding
	 * 
	 * @param sData
	 * @param iLength
	 * @param sPadStr
	 * @param sCharset
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String rightPadByByteLenth(String sData, int iLength, String sPadStr, String sCharset) throws UnsupportedEncodingException {

		if (sData == null) {
			return null;
		}

		if (sPadStr == null || sPadStr.length() == 0) {
			sPadStr = " ";
		}
		String sResult = sData;

		try {
			while (sResult.getBytes(sCharset).length < iLength) {
				sResult += sPadStr;
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw e;
		}

		return sResult;
	}

	/**
	 * 移除字串右邊之全形空白
	 * 
	 * @param sValue
	 * @return
	 */
	public static String trimRightBigSpace(String sValue) {
		String sResult = sValue;

		while (sResult.endsWith("　")) {
			sResult = sResult.substring(0, sResult.length() - 1);
		}

		return sResult;
	}

	/**
	 * 將StringArray轉換為SQL的Where In格式 ex : StringUtils.StrArray2WhereInSql(["A", "B", "C"]) = "'A','B','C'"
	 * 
	 * @param array
	 * @param sSeparator
	 * @return
	 */
	public static String strArray2WhereInSql(String[] array) {
		if (array == null) {
			return null;
		}

		String sSeparator = ",";

		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < array.length; i++) {
			if (i != 0) {
				sb.append(sSeparator);
			}
			sb.append("'").append(StringUtils.defaultString(array[i])).append("'");
		}

		return sb.toString();
	}

	/**
	 * Parse the given <code>localeString</code> into a {@link Locale}.
	 * <p>
	 * This is the inverse operation of {@link Locale#toString Locale's toString}.
	 * 
	 * @param localeString the locale string, following <code>Locale's</code> <code>toString()</code> format ("en", "en_UK", etc); also accepts spaces as
	 *        separators, as an alternative to underscores
	 * @return a corresponding <code>Locale</code> instance
	 */
	public static Locale parseLocaleString(String localeString) {
		String[] parts = tokenizeToStringArray(localeString, "_ ", false, false);
		String language = (parts.length > 0 ? parts[0] : "");
		String country = (parts.length > 1 ? parts[1] : "");
		String variant = "";
		if (parts.length >= 2) {
			// There is definitely a variant, and it is everything after the country
			// code sans the separator between the country code and the variant.
			int endIndexOfCountryCode = localeString.indexOf(country) + country.length();

			// Strip off any leading '_' and whitespace, what's left is thev ariant.
			variant = trimLeadingWhitespace(localeString.substring(endIndexOfCountryCode));
			if (variant.startsWith("_")) {
				variant = trimLeadingCharacter(variant, '_');
			}
		}
		return (language.length() > 0 ? new Locale(language, country, variant) : null);
	}

	public static String[] tokenizeToStringArray(String str, String delimiters) {
		return tokenizeToStringArray(str, delimiters, true, true);
	}

	/**
	 * Parse the given <code>localeString</code> into a {@link Locale}.
	 * <p>
	 * This is the inverse operation of {@link Locale#toString Locale's toString}.
	 * 
	 * @param localeString the locale string, following <code>Locale's</code> <code>toString()</code> format ("en", "en_UK", etc); also accepts spaces as
	 *        separators, as an alternative to underscores
	 * @return a corresponding <code>Locale</code> instance
	 */
	public static String[] tokenizeToStringArray(String str, String delimiters, boolean trimTokens, boolean ignoreEmptyTokens) {

		if (str == null) {
			return null;
		}
		StringTokenizer st = new StringTokenizer(str, delimiters);
		List<String> tokens = new ArrayList<String>();
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (trimTokens) {
				token = token.trim();
			}
			if (!ignoreEmptyTokens || token.length() > 0) {
				tokens.add(token);
			}
		}
		return toStringArray(tokens);
	}

	public static String[] toStringArray(Collection<String> collection) {
		if (collection == null) {
			return null;
		}
		return (String[]) collection.toArray(new String[collection.size()]);
	}

	/**
	 * Trim leading whitespace from the given String.
	 * 
	 * @param str the String to check
	 * @return the trimmed String
	 * @see java.lang.Character#isWhitespace
	 */
	public static String trimLeadingWhitespace(String str) {
		if (!hasLength(str)) {
			return str;
		}
		StringBuffer buf = new StringBuffer(str);
		while (buf.length() > 0 && Character.isWhitespace(buf.charAt(0))) {
			buf.deleteCharAt(0);
		}
		return buf.toString();
	}

	/**
	 * Trim all occurences of the supplied leading character from the given String.
	 * 
	 * @param str the String to check
	 * @param leadingCharacter the leading character to be trimmed
	 * @return the trimmed String
	 */
	public static String trimLeadingCharacter(String str, char leadingCharacter) {
		if (!hasLength(str)) {
			return str;
		}
		StringBuffer buf = new StringBuffer(str);
		while (buf.length() > 0 && buf.charAt(0) == leadingCharacter) {
			buf.deleteCharAt(0);
		}
		return buf.toString();
	}

	/**
	 * Check that the given String is neither <code>null</code> nor of length 0. Note: Will return <code>true</code> for a String that purely consists
	 * of whitespace.
	 * <p>
	 * 
	 * <pre>
	 * StringUtils.hasLength(null) = false
	 * StringUtils.hasLength(&quot;&quot;) = false
	 * StringUtils.hasLength(&quot; &quot;) = true
	 * StringUtils.hasLength(&quot;Hello&quot;) = true
	 * </pre>
	 * 
	 * @param str the String to check (may be <code>null</code>)
	 * @return <code>true</code> if the String is not null and has length
	 * @see #hasText(String)
	 */
	public static boolean hasLength(String str) {
		return (str != null && str.length() > 0);
	}

	/**
	 * Get Mask String
	 * 
	 * @param orign
	 */
	public static String toMaskString(String orign) {
		if (!isBlank(orign) && orign.length() > 5) {
			orign = substring(orign, 0, 2) + "***" + substring(orign, 5);
		}
		return orign;
	}

	public static String getBase64Encode(String s) {
		if (s == null)
			return null;
		return (new sun.misc.BASE64Encoder()).encode(s.getBytes());
	}

	public static String getBase64Encode(String s, String encoding) throws Exception {
		if (s == null)
			return null;
		return (new sun.misc.BASE64Encoder()).encode(s.getBytes(encoding));
	}

	public static String getBase64Decode(String s) {
		if (s == null)
			return null;
		sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
		try {
			byte[] b = decoder.decodeBuffer(s);
			return new String(b);
		} catch (Exception e) {
			return null;
		}
	}

	public static String getBase64Decode(String s, String encoding) {
		if (s == null)
			return null;
		sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
		try {
			byte[] b = decoder.decodeBuffer(s);
			return new String(b, encoding);
		} catch (Exception e) {
			return null;
		}
	}

	public static String defaultString(String s) {
		if (s != null)
			s = s.trim();
		return org.apache.commons.lang.StringUtils.defaultString(s);
	}
	
    public static boolean isEmpty(String str){
    	if(str==null) return true;
    	if(str.trim().equals("")) return true;
    	
    	return false;
    }
    
    /**
     * 將數字轉為金額格式
     * 
     * 例  1234500  轉成  1,234,500
     * 
     * @param str
     * @return
     * @author Shan
     */
    public static String getCoinStr(String str){
    	
    	StringBuffer coin = new StringBuffer();
    	
    	String comma = ",";
		int len = str.length();
		int spot = 0;
		
		String minus = "";
		String decimal = ".";
		
		if(str.indexOf("-") != -1){
			minus = str.substring(0, 1);
			str = str.substring(1);
		}
		
		if(len < 3){
			return minus + str;
		}
		
		if (len % 3 == 0) {

			for (int i = 1; i <= len / 3; i++) {

				coin.append(str.substring(spot, spot = spot + 3));

				if (i != len / 3) {
					coin.append(comma);
				}
			}
		} else if (len % 3 == 1) {

			for (int i = 0; i <= len / 3; i++) {

				if (i == 0) {
					coin.append(str.substring(spot, spot = spot + 1));
					coin.append(comma);
					continue;
				}

				coin.append(str.substring(spot, spot = spot + 3));

				if (i != len / 3) {
					coin.append(comma);
				}
			}

		} else if (len % 3 == 2) {

			for (int i = 0; i <= len / 3; i++) {

				if (i == 0) {
					coin.append(str.substring(spot, spot = spot + 2));
					coin.append(comma);
					continue;
				}

				coin.append(str.substring(spot, spot = spot + 3));

				if (i != len / 3) {
					coin.append(comma);
				}
			}
		}
    	return minus + coin.toString();
    }
    
    /**
     * 左補字串
     * 
     * @param str 原始字串
     * @param len 長度 
     * @param ch 要補上的字 
     * @return 
     * @author Jenny
     */
    public static String fillLeft(String str, int len, String ch) {
	    if (str.length() < len) {
	      for (int i = str.length(); i < len; i++) {
	        str = ch + str;
	      }
	    }
	    return str;
	}
	
    public static String checkPersonalId(String identification){
        char firstChar = identification.toUpperCase().charAt(0); 
        //A-Z的對應數字
        int idum[] = {10,11,12,13,14,15,16,17,34,18,19,20,21
                        ,22,35,23,24,25,26,27,28,29,32,30,31,33};
    	
        //轉成11碼的字串,'A'=65； substring:從index:1開始取String
        identification = idum[firstChar - 'A'] + identification.substring(1);
        
        //把第一碼放到sum中,'0'=48
        int sum = identification.charAt(0) - '0';
        
        //取2-10的總合
        for(int i=1; i<10; i++) {
        	 sum += identification.charAt(i)*(10-i);
        }
         
        //10-加總的尾數 = 第11碼
        int checksum = (10-(sum%10))%10;
        if (checksum == identification.charAt(10) - '0') {
        	return "PASS";
        } else {
        	return "FAIL";
        }
      }
     
    
    public static boolean checkCompanyId(String identification){
    	 if (identification == null || identification.length() != 8) {
    		 return false;
    	 }
    	 
    	 boolean result = false;
    	 String  logic_number_multiplier =  "12121241";
    	 
    	 boolean seventhNum = false; //第七個數是否為七
    	 if (TW_COMPANY_ID_PATTERN.matcher(identification).matches()) {
    		 int tmp = 0, sum = 0;
    		 for (int i = 0; i < 8; i++) {
    			 tmp = (identification.charAt(i) - '0') * (logic_number_multiplier.charAt(i) - '0');
    			 sum += (int) (tmp / 10) + (tmp % 10); //取出十位數和個位數相加
    			 if (i == 6 && identification.charAt(i) == '7') {
    				 seventhNum = true;
    			 }
    		}
    		 
    		 if (seventhNum) {
        		 if ( (sum % 10) == 0 || ( (sum + 1) % 10) == 0) {
        			//如果第七位數為7
        			 result = true;
        		 } 
        	 } else {
        		 if ((sum % 10) == 0) {
        			 result = true;
        	     }
        	 }
    	 }
    		 return result;
       }
}
