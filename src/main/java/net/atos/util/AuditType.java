package net.atos.util;

import java.io.Serializable;

public enum AuditType implements Serializable {
	Login("0", "登入"),
	Logout("1", "登出"),
	Add("2", "新增"),
	Update("3", "修改"),
	Query("4", "查詢"),
	Delete("5", "刪除"),
	JobStart("6","啟動排程"),
	JobStop("7","停止排程"),
	Unknown("8", "Unknown");

	private String type;
	private String description;

	private AuditType(final String type, final String description) {
		this.type = type;
		this.description = description;
	}

	public String toString() {
		return description;
	}

	public static AuditType fromString(final String type) {
		return byType(type);
	}

    public static AuditType byType(final String type) {
        for (AuditType e : AuditType.values()) {
            if (e.type.equals(type)) {
                return e;
            }
        }
        return Unknown;
    }

    public String getType() {
    	return this.type;
    }
}