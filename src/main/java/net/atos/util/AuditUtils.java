package net.atos.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.atos.audit.bean.Audit;
import net.atos.user.bean.LoginInfo;

public class AuditUtils {

	public static final String LOGIN = "LoginAction/loginAuthenticaion.do";
	public static final String LOGOUT = "LogoutAction/logoutAction.do";
	
	
	/**
	 * 整理Audit資料
	 * 
	 * @param userInfo 登入者資訊
	 * @param obj java類別
	 * @param auditType 動作
	 * @param desc 說明
	 * @param sqlLog SQL語法
	 * @param content 異動的Bean物件
	 * @return
	 */
	public static Audit getAudit(LoginInfo loginInfo,String programName, AuditType auditType, String implFunction, String sqlLog, String note) {

		Audit audit = new Audit();

		audit.setUuid(UUID.randomUUID().toString().replaceAll("-", ""));
		audit.setUserId(loginInfo.getUserId());
		audit.setAuditTime(new Date());
		audit.setProgramName(programName);
		audit.setAuditType(auditType.getType());
		audit.setClientIpAddr(loginInfo.getClientIpAddr());
		audit.setImplFunction(implFunction);
		audit.setSqlLog(sqlLog);
		audit.setNote(note);

		return audit;
	}

	/**
	 * 將Object bean 轉成json存入Audit
	 * 
	 * @param oldBean  異動前的物件
	 * @param newBean  異動後的物件
	 * @return
	 */
	public static Map<String, Object> getContentMap(Object oldBean, Object newBean){
		
		Map<String, Object> content = new HashMap<String, Object>();
		
		content.put("oldObject", oldBean==null?"":oldBean);
		content.put("newObject", newBean==null?"":newBean);
		
		return content;
	}
	
}
