package net.atos.util;


import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/***
 * some useful tools
 * @author cjfan
 *
 */

public class Util {

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    public static final SimpleDateFormat PageDateFormat = new SimpleDateFormat("yyyy/MM/dd");
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat sybaseDateTimeFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    private static String defaultDatePattern = "yyyy-MM-dd";
    
    
    
    /**
     * 099/09/26 17:23:12 -> 2010-09-26 17:23:12
     * 099/09/26          -> 2010-09-26
     * kent.fan amended. 
     * Accept date or datetime format conversion
     * @param twDateStr
     * @return
     */
    public static String convertTwDate2EnDate(String twDateStr){
        try{
            if(twDateStr.length() == 18 || twDateStr.length() == 9){
                String returnStr = String.valueOf(Integer.parseInt(twDateStr.substring(0, 3)) + 1911) + "-" + twDateStr.substring(4,6)
                +"-"+twDateStr.substring(7); 
                return returnStr;
            }
        }catch(Exception e){
            e.getMessage();
        }
        return null;
    }
    
    /**
     * 將西元年轉成民國年
     * @param enDateStr
     * @return
     */
    public static String convertEnDate2TwDate(String enDateStr){
        try{
            if(enDateStr.length() == 19 || enDateStr.length() == 10){
                String twYear = String.valueOf(Integer.parseInt(enDateStr.substring(0, 4)) - 1911);
                if (twYear.length()==2){
                    twYear="0"+twYear;
                }
                String returnStr = twYear + enDateStr.substring(4); 
                return returnStr;
            } else if (enDateStr.length()==0||enDateStr==null){
                return enDateStr;
            }
        }catch(Exception e){
            e.getMessage();
        }
        return null;
    }
    
    /**
     * 2010/09/26 17:23:12 -> java.util.Date
     * @param enDateStr
     * @return
     */
    public static java.util.Date getDateFromStr(String enDateStr){
        try {
            return dateFormat.parse(enDateStr);
            
        } catch (ParseException e) {
        	e.getMessage();
        }
        return null;
    }
    
    /**
     * 2010-09-26 17:23:12 -> java.util.Date
     * @param enDateStr
     * @return
     */
    public static java.util.Date getDateOnlyFromStr(String enDateStr){
        try {
            return sdf.parse(enDateStr);
            
        } catch (ParseException e) {
        	e.getMessage();
        }
        return null;
    }
    
    /**
     * java.util.Date -> 2011/12/12 19:31:33
     * @param date
     * @return
     */
    public static String convertDate2Str(java.util.Date date){
        try{
            return dateFormat.format(date);
        }catch(Exception e){
        	e.getMessage();
        }
        return "error";
    }
    
    /**
     * 根據資料實體動態塞值給表單
     * @param account
     */
    public static void setDataToOhterBean(Object otherBean, Object dataBean, Class<? extends Object> classType){
        Object[] oba = null;
        Method[] m = classType.getDeclaredMethods();
        for(int i=0; i<m.length; i++){
            if(m[i].toString().indexOf(".get") > 0){
                try {
                    Object o = m[i].invoke(dataBean, oba);
                    Method mm = otherBean.getClass().getMethod("set"+m[i].getName().substring(3), new Class[]{m[i].getReturnType()});                    
                    mm.invoke(otherBean, new Object[]{o});
                } catch(NoSuchMethodException e){
                    //沒有method意味著表單沒有相同欄位 do nothing
                } catch (Exception e) {
                	e.getMessage();
                    e.printStackTrace();
                } 
            }
        }
    }
    
    /**
     * 根據表單的值動態塞回dataBean(塞回前需做資料檢核)
     * @param formBean
     * @param dataBean
     * @param classType
     * @throws Exception
     */
    public static void updateChangedFromOhterBean(Object otherBean, Object dataBean, Class<? extends Object> classType) throws Exception{
        Object[] oba = null;
        Method method = null;
        Method[] methodArr = classType.getMethods();
        Object originalVal;
        Object formVal;
        
        for(int i=0; i<methodArr.length; i++){
            if(methodArr[i].toString().indexOf(".get") > 0){
                //找尋表單有值的欄位
                originalVal = methodArr[i].invoke(classType.cast(dataBean), oba);
               
                try{
                    method = otherBean.getClass().getMethod(methodArr[i].getName(), new Class[]{});
                }catch(NoSuchMethodException e){
                    //do nothing
                    method = null;
                }
                if(method != null){
                    formVal = method.invoke(otherBean, oba);
                    
                    //如果兩者不同則更新
                    if(((originalVal == null && formVal != null) || (originalVal != null && !originalVal.equals(formVal))) && !methodArr[i].getName().equals("getClass")){
                        method = classType.getMethod("set"+methodArr[i].getName().substring(3), methodArr[i].getReturnType());
                        method.invoke(classType.cast(dataBean), new Object[]{formVal});
                    }
                }
            }
        }
    }
    
    /**
     * 亂數密碼產生器
     * @return
     */
    public static String getRandomPwd() {
        java.util.Random r = new java.util.Random();
        int rnd = 0;
        String pwd = "";
        for (int x = 0; x < 10; x++) {
            rnd = r.nextInt(94) + 33;
            pwd = pwd + (char)rnd;
        }
        return pwd;
    }
    
    /**
     * vic add
     * 日期格式轉換 095/03/08 -> 2006/03/08
     * @param dateStr
     * @return String
     */
    public static String transADDateString(String dateStr) {
        String tmpStr = null;

        try {
          if (dateStr.trim().length() == 9) {
            tmpStr = String.valueOf(Integer.parseInt(dateStr.substring(0, 3)) +
                                    1911) + dateStr.substring(3, 9);
          }
          else {
            tmpStr = dateStr;
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        finally {
          return tmpStr;
        }
      }
    
    /**
     * vic add
     * 日期格式轉換 095/03/08 -> 2006/03/08
     * @param dateStr
     * @return Date
     */
    public static Date transADDate(String dateStr) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");     
        String tmpStr = null;
        Date applyDateTemp = new Date();
        try {
          if (dateStr.trim().length() == 9) {
            tmpStr = String.valueOf(Integer.parseInt(dateStr.substring(0, 3)) +
                                    1911) + dateStr.substring(3, 9);
            applyDateTemp = dateFormat.parse(tmpStr);
          }else {
//            tmpStr = dateStr;
              applyDateTemp = null;
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        finally {
          return applyDateTemp;
        }
      }
    

    
    public static String format(Date date, String pattern){ 
        return date == null ?  " " : new   SimpleDateFormat(pattern).format(date);  
    } 
    
    /**
     * vic add
     * 日期格式轉換 2006/03/08 -> 095/03/08
     * @param Date
     * @return dateStr
     */
    public static String transDateString(Date date) {       
        String tmpStr = null;
        System.out.println("date===="+date);
        try {
            /**
             * 從資料庫轉出來的格式是"2010-11-24"
             */
            String dateStr = format(date, defaultDatePattern);

            System.out.println("dateStr===="+dateStr);
          if (dateStr.trim().length() == 10) {
              System.out.println(">>>>"+dateStr.trim().length());
            String yyyStr = String.valueOf(Integer.parseInt(dateStr.substring(0,4)) - 1911);
            if(yyyStr.length()<3){
                yyyStr = "0" + yyyStr;
            }
            String mmStr = dateStr.substring(5,7);
            if(mmStr.length()<2){
                mmStr = "0" + mmStr;
            }
            String ddStr = dateStr.substring(8,10);
            if(ddStr.length()<2){
                ddStr = "0" + ddStr;
            }
            tmpStr = yyyStr+"/"+mmStr+"/"+ddStr;
            
          }else {
              System.out.println(">>>>"+dateStr.trim().length());
              tmpStr = "";
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        finally {
          return tmpStr;
        }
      }

    /**
     * 99/03/21 13:21:10 -> 2010-03-21 13:21:10
     */
    public static String convertRoc2WesternCalendar(String rocCalendarString){

        rocCalendarString = rocCalendarString.replaceAll("/","-");
        int yearIndex = rocCalendarString.indexOf("-");
        rocCalendarString.substring(0,yearIndex);
        int westernYear = Integer.parseInt(rocCalendarString.substring(0,yearIndex))+1911;
        String westernCalendarString = westernYear+rocCalendarString.substring(yearIndex); 
        return westernCalendarString;

    }
    
    /**
     * 判斷憑證是否有效
     * @param signpkcs
     * @return
     */
    public static boolean isValidCA(String signpkcs){
        //FIXME 宗偉
        return true;
    }
    
    /**
     * 取得所選當月第一天
     * @param calendar
     * @return
     */
    public static Date getFirstMonthDay(Calendar calendar) {
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        return calendar.getTime();
    }
    
    /**
     * 取得所選當月最後一天
     * @param calendar
     * @return
     */
    public static Date getLastMonthDay(Calendar calendar) {
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }
    
    

    public static String getEnDate(Calendar calendar) {
    	String day = "";
    	int year = calendar.get(Calendar.YEAR);
    	int month = calendar.get(Calendar.MONTH) + 1;
    	int date = calendar.get(Calendar.DATE);
    	String strYear = toDefiniteLengthString(year, 4, '0').trim();
    	String strMonth = toDefiniteLengthString(month, 2, '0').trim();
    	String strDate = toDefiniteLengthString(date, 2, '0').trim();
    	day = strYear + "/" + strMonth + "/" + strDate;
    	return day;
    }
    
    /**
     * 數字轉字串補零
     * @param l
     * @param iLength
     * @param ch
     * @return
     */
    public   static   String   toDefiniteLengthString(long   l,   int   iLength,   char   ch)   { 
        StringBuffer   sb   =   new   StringBuffer( " "); 
        String   strValue   =   String.valueOf(l); 
        if   (iLength   <   strValue.length())   { 
                return   null; 
        } 
        for   (int   i   =   0;   i   <   iLength   -   strValue.length();   i++)   { 
                sb.append(ch); 
        } 
        sb.append(l); 
        return   new   String(sb); 
    }
    
    /**
     * 國際機場(for 權限)
     * @param code
     * @return
     */
    public static Map<String, String> getAirPortList(String code) {
    	Map<String, String> map = new LinkedHashMap<String, String>();
    	if (code == null) {code = "";}
    	for (int i = 0 ; i < 18; i++) {
    		if (i == 0 && ( code.equals("10") || code.equals("6") || code.equals("9") ) ) {
    			map.put("TPE", "桃園");
    			map.put("TPE1", "桃園一航廈");
    			map.put("TPE2", "桃園二航廈");
    		} else if (i == 1 && ( code.equals("10") || code.equals("6") || code.equals("5") ) ) {
    			map.put("TSA", "松山");   			
    		} else if (i == 2 && ( code.equals("10") || code.equals("6") || code.equals("7") ) ) {
    			map.put("RMQ", "台中");   			
    		} else if (i == 3 && ( code.equals("10") || code.equals("6") || code.equals("8") || code.equals("9") ) ) {
    			map.put("KHH", "高雄");   			
    		} else if (i == 4 && ( code.equals("10") || code.equals("6") || code.equals("11") ) ) {
    			map.put("HUN", "花蓮");
    		} else if (i == 5 && ( code.equals("10") || code.equals("6") || code.equals("12") ) ) {
    			map.put("MZG", "澎湖馬公");
    			map.put("CMJ", "澎湖七美");
    			map.put("WOT", "澎湖望安");
    		} else if (i == 6 && ( code.equals("10") || code.equals("6") || code.equals("13") ) ) {
    			map.put("KNH", "金門");
    		} else if (i == 7 && ( code.equals("10") || code.equals("6") || code.equals("14") ) ) {
    			map.put("TTT", "台東");
    		} else if (i == 8 && ( code.equals("10") || code.equals("6") || code.equals("15") ) ) {	
    			map.put("TNN", "台南");    			
    		} else if (i == 9 && ( code.equals("10") || code.equals("6") || code.equals("16") ) ) {	
    			map.put("CYI", "嘉義");
    		} else if (i == 10 && ( code.equals("10") || code.equals("6") || code.equals("17") ) ) {	
    			map.put("PIF", "屏東");
    		} else if (i == 11 && ( code.equals("10") || code.equals("6") || code.equals("18") ) ) {	
    			map.put("HCN", "恆春");
    		} else if (i == 12 && ( code.equals("10") || code.equals("6") || code.equals("19") ) ) {	
    			map.put("GNI", "綠島");
    		} else if (i == 13 && ( code.equals("10") || code.equals("6") || code.equals("20") ) ) {	
    			map.put("KYD", "蘭嶼");
    		} else if (i == 14 && ( code.equals("10") || code.equals("6") || code.equals("21") ) ) {	
    			map.put("MFK", "馬祖北竿");
    			map.put("LZN", "馬祖南竿");
    		} else if (i == 16 && ( code.equals("10") || code.equals("6") || code.equals("22") ) ) {	
    			map.put("QBL", "東沙");
    		} else if (i == 17 && ( code.equals("10") || code.equals("9") || code.equals("6") ) ) {
    			map.put("all", "全部");  			
    		} 
    	}
    	return map;
    }
    
    /**
     * 取得性別
     * @return
     */
    public static Map<String, String> getGenderList() {
    	Map<String, String> map = new LinkedHashMap<String, String>();
    	map.put("all", "全部");
    	map.put("M", "男");
    	map.put("F", "女");
    	map.put("X", "未知");
    	return map;
    }
    
    /**
     * 取得傳送狀態
     * @return
     */
    public static Map<String, String> getIsOverTimeList() {
    	Map<String, String> map = new LinkedHashMap<String, String>();
    	map.put("all", "全部");
    	map.put("P", "無旅客資料");
    	map.put("N", "逾期未傳送");
    	map.put("D", "遲送且資料未傳送完成");
    	map.put("F", "遲送且資料已傳送完成");
    	map.put("S", "異常");
    	return map;
    }
    
    public static String pollStatus(Map<String,Object> inputParamMap) {
    	String st = "";
    	
    	String stepStatusAll = inputParamMap.get("statusAll")==null? null: (String) inputParamMap.get("statusAll");

		List<String> osl = Arrays.asList("FAILED","ABANDONED","UNKNOWN","STOPPED","Step Aborted");
		List<String> rsl = Arrays.asList("STARTING", "STARTED", "STOPPING");
		
    	String statusResult = stepStatusAll;
		
		if (statusResult != null) {
			List<String> stsList = Arrays.asList(((String) statusResult).split(","));
			if (stsList != null) {
				for (String obj7s : stsList) {
					if (osl.contains(obj7s)){
						st = "FAILED";
						break;
					} else if (rsl.contains(obj7s)) {
						st = "RUNNING";
						break;
					} else {
						st = obj7s;
					}
				}
			}
		}

		if (st.equalsIgnoreCase("Step finished")) st="COMPLETED";
		if (st.equalsIgnoreCase("Step aborted")) st="FAILED";
    	
    	return st;
    }
    
}
