package net.atos.interceptor;


import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ParameterAware;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;

import net.atos.user.bean.LoginInfo;

import net.atos.util.PageParameter;
import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport implements SessionAware, RequestAware, ParameterAware {
	private static final long serialVersionUID = -2747166583979807628L;

	protected final static String SYS_MSG_Control = "net.atos.util.Messages";
	protected static Map<String, Object> requestMap;
	protected Map<String, Object> sessionMap;
	protected Map<String, String[]> parameterMap;
	protected static Map<String, Object> jsonMap;
	public HttpServletRequest request;
	protected static HttpURLConnection conn;
	

	/**
	 * 依PageParameter參數，顯示list資料
	 * 
	 * @param dataList
	 * @param page
	 * @return
	 * 
	 * @author kent
	 * @since 2013/07/09
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List dataPagination(List dataList, PageParameter page){
		
		List list = new ArrayList();
		
		int currentPageNo = page.getCurrentPageNo();
		int pageSize = page.getPageSize();
		int totalPage = dataList.size() / pageSize + 1;
		
		int firstResult = (currentPageNo - 1) * pageSize;
		int lastResult = currentPageNo * pageSize;
		
		if(dataList.size() > pageSize){
			if (currentPageNo == totalPage) {
				for (int i = firstResult; i < dataList.size(); i++) {
					list.add(dataList.get(i));
				}
			}else{
				for (int i = firstResult; i < lastResult; i++) {
					list.add(dataList.get(i));
				}
			}
		}else{
			list = dataList;
		}
		return list;
	}
	
	public LoginInfo getLoginInfo() {

		HttpServletRequest req = ServletActionContext.getRequest();

		String user = (String) req.getSession().getAttribute("loggedInUser");
		String ip = (String) req.getSession().getAttribute("loginIp");
		String password = (String) req.getSession().getAttribute("loginPwd");

		LoginInfo loginInfo = new LoginInfo();

		loginInfo.setUserId(user);
		loginInfo.setClientIpAddr(ip);
		loginInfo.setPassword(password);
		
		return loginInfo;
	}

	
	public void setParameters(Map<String, String[]> arg0) {
		// TODO Auto-generated method stub
		
	}

	
	public void setRequest(Map<String, Object> requestMap) {
		this.requestMap = requestMap;
	}
	
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

}
